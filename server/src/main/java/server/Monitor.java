package server;

public class Monitor {
    private int clientsNumber = 0;
    private int id = 0;
    private ServerMain server;

    public Monitor(ServerMain server){
        this.server = server;
    }

    public void newClient(){
        clientsNumber++;
        id++;
    }

    public void disconnect(){
        clientsNumber--;
    }
    public int getClientsNumber(){
        return clientsNumber;
    }
    public int getId(){
        return id;
    }
}