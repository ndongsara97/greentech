package server;

import java.net.Socket;
import java.net.ServerSocket;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import connection.pool.*;
import util.PropertyReader;

public class ServerMain {
	private ServerSocket serverSocket;
	private final Logger serverLog = LogManager.getLogger(ServerMain.class);

	public void start(int port){
		new PropertyReader();
		new DataSource();
		Monitor monitor = new Monitor(this);

		try {
			serverSocket = new ServerSocket(port);
			serverLog.info("Server started");

			Socket clientSocket;

			while (true) {
				synchronized (monitor){
					while(DataSource.isEmpty()){
						monitor.wait();
					}
					//By this point the connection pool contains at least one connection

					clientSocket = serverSocket.accept();
					serverLog.info("New client connected");
					monitor.newClient();

					//The connection is obtained within the constructor so keep this part within the synchronized
					new ThreadClient(clientSocket, monitor, monitor.getId()).start();

					monitor.notifyAll();
				}
				Thread.sleep(1000); //To leave some room for the scheduler
			}
		} catch (IOException e){
			serverLog.info("Server socket closed");
		} catch (InterruptedException ie){
			serverLog.info("Error while sleeping in main loop", ie);
		} finally {
			stop();
		}
	}

	public void stop() {
		DataSource.closePool();
		try {
			serverSocket.close();
		} catch (IOException e) {
			serverLog.error("Error when closing server socket",e);
		}
		serverLog.info("Server offline");
		System.exit(0);
	}

	public static void main(String[] args) {
		ServerMain server=new ServerMain();
		server.start(6666);
		server.stop();
	}
}