package server;

import connection.queries.core.QueryDecoder;
import connection.queries.core.QueryEncoder;
import connection.queries.core.QueryHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class ThreadClient extends Thread {
    private final Monitor monitor;
    private final Socket clientSocket;
    private final QueryEncoder encoder = new QueryEncoder(this);
    private final QueryHandler handler = new QueryHandler(encoder);
    private final QueryDecoder decoder = new QueryDecoder(handler,encoder);
    private PrintWriter output;
    private BufferedReader input;
    private final Logger threadClientLog = LogManager.getLogger(ThreadClient.class);

    ThreadClient(Socket clientSocket, Monitor monitor, int id) {
        this.monitor = monitor;
        this.clientSocket = clientSocket;
        this.setName("Client " + id);

        try {
            output = new PrintWriter(clientSocket.getOutputStream(),false);
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            threadClientLog.error("Error trying to set up socket streams",e);
        }
        handler.getConnection();
    }

    public void run() {
        try {
            String request;
            while(clientSocket.isConnected()){
                request = input.readLine();
                threadClientLog.trace("Request received from "+this.getName());
                decoder.decodeOperation(request);
            }
            disconnect();
        } catch (Exception e) {
            disconnect();
        }
    }

    public void disconnect(){
        synchronized (monitor) {
            try {
                clientSocket.close();
            } catch (IOException ioe){
                threadClientLog.error("Error when disconnecting client socket",ioe);
            }
            handler.releaseConnection();
            monitor.disconnect();
            threadClientLog.info(this.getName()+ " disconnected");
            monitor.notifyAll();
        }
    }

    public void sendResponse(String response){
        output.println(response);
        output.flush();
        threadClientLog.trace("Response sent to "+this.getName());
    }
}
