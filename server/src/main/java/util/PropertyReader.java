package util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    private String user,password,url;
    private int poolMaxSize;
    private final Logger propertyLog = LogManager.getLogger(PropertyReader.class);

    public PropertyReader(){
        Properties prop = new Properties();
        try {
            InputStream input = new FileInputStream(System.getenv("etc")+"/pool.properties");
            prop.load(input);

            user = prop.getProperty("user");
            password = prop.getProperty("password");
            url = prop.getProperty("url");
            poolMaxSize = Integer.parseInt(prop.getProperty("pool_max_size"));


        } catch(IOException ioe){
            propertyLog.error("Error while loading property file",ioe);
        }
    }

    public String getUser(){
        return user;
    }
    public String getPassword(){
        return password;
    }
    public String getUrl(){
        return url;
    }
    public int getPoolMaxSize(){
        return poolMaxSize;
    }
}