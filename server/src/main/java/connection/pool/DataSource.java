package connection.pool;

import java.sql.Connection;

public class DataSource {

	private final static PoolImplementation jdbcPool = new PoolImplementation();
	public static Connection getConnection() {return jdbcPool.getConnection();}
	public static void releaseConnection(Connection connect) {jdbcPool.addConnection(connect);}
	public static void closePool() {jdbcPool.closeAll();}
	public static int getPoolSize(){return jdbcPool.getSize();}
	public static boolean isEmpty(){return getPoolSize()==0;}
}