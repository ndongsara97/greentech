package connection.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.postgresql.Driver;
import util.PropertyReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class PoolImplementation {

	private final LinkedHashSet<Connection> pool = new LinkedHashSet<>();
	private final Logger poolLog = LogManager.getLogger(PoolImplementation.class);

	PoolImplementation() {
		new Driver();
		PropertyReader properties = new PropertyReader();
		int maxSize = properties.getPoolMaxSize();
		try {
			for (int  i=0; i<maxSize; i++) {
				Connection connect = DriverManager.getConnection(
						properties.getUrl(),
						properties.getUser(),
						properties.getPassword());
				addConnection(connect);
			}
		} catch (SQLException e) {
			poolLog.error("Error when initializing connection pool",e);
		}
	}
	
	public void addConnection(Connection connect) {
		pool.add(connect);
	}

	public Connection getConnection() {
		if(!pool.isEmpty()) {
			Connection c = pool.iterator().next();
			pool.remove(c);
			return c;
		} else {
			poolLog.info("Null connection");
			return null;
		}
	}
	
	public void closeAll() {
		try {
			Iterator<Connection> iter1= pool.iterator();
			Connection connect = null;
			while(iter1.hasNext()) {
				connect = iter1.next();
				if (connect != null) connect.close();
			}
			poolLog.info("Connection pool closed successfully");
		} catch (SQLException sqle) {
			poolLog.error("Error when trying to close connections in pool",sqle);
		}
	}

	public int getSize(){return pool.size();}
}