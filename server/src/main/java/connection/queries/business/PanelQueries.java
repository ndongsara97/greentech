package connection.queries.business;

import connection.queries.core.QueryEncoder;
import connection.queries.core.QueryHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PanelQueries {

    private final Logger log = LogManager.getLogger(StatsQueries.class);
    QueryHandler handler;
    QueryEncoder encoder;

    public PanelQueries(QueryHandler handler, QueryEncoder encoder){
        this.handler = handler;
        this.encoder = encoder;
    }

    public void selectParcelsById(int idParcelle, int energy){

        //String[][] data = selectQuery("SELECT * FROM parcelle order by parcelle_id ASC;");
        String query = "SELECT taux_ensoleillement, possibilite_placement_panneau FROM parcelle where parcelle_id = {idParcel};";
        query = query.replace("{idParcel}", String.valueOf(idParcelle));
        String[][] data = handler.selectQuery(query);// TAUX & POSSIBILITE
        switch(data[0][1]){
            case "t" -> {
                int engSinglePane = 1000*Integer.parseInt(data[0][0])/100;
                int nbrPane = energy/engSinglePane;
                String [][] finalData = new String[1][1];
                finalData[0][0] = String.valueOf(nbrPane);
                encoder.encodePaneNbr(finalData, "select_parcels_by_id");
            } case "f" -> {
                String [][] finalData = new String[1][1];
                finalData[0][0] = "0";
                encoder.encodePaneNbr(finalData, "select_parcels_by_id");
            }
        }
    }
    public void setFirstIdPanel(int idPanel){
        String query = "select id_panel from panneaux ORDER BY id_panel DESC limit 1;";
        String [][] data = handler.selectQuery(query);
        encoder.encodeQuery(data, "setFirstIdPanel");
    }


    public void getPaneParcels() {
        String[][] data = handler.selectQuery("select possibilite_placement_panneau, longitude, latitude, parcelle_id from parcelle order by parcelle_id ASC;");
        encoder.encodeQuery(data, "select_pane_parcels");
    }

    public void panelInsert(int idPanel, int x, int y, int idParcels) {
        try {
            PreparedStatement pstmt = handler.getPreparedStatement("insert into panneaux (id_panel,x,y,id_parcels) values (?,?,?,?)");
            pstmt.setInt(1, idPanel);
            pstmt.setInt(2, x);
            pstmt.setInt(3, y);
            pstmt.setInt(4, idParcels);
            int linesChanged = pstmt.executeUpdate();
            handler.commit();
            pstmt.close();
        } catch (SQLException sqle) {
            log.error("Error while trying to execute insert query", sqle);
        }
    }

}
