package connection.queries.business;

import connection.queries.core.QueryEncoder;
import connection.queries.core.QueryHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class StatsQueries {
    private final Logger statsQueriesLog = LogManager.getLogger(StatsQueries.class);
    QueryHandler handler;
    QueryEncoder encoder;

    public StatsQueries(QueryHandler handler, QueryEncoder encoder){
        this.handler = handler;
        this.encoder = encoder;
    }

    public boolean notInsertedToday(String measure){
        String query = "SELECT COUNT(*) FROM statistic WHERE measure = '{measure}' AND date = DATE(NOW());";
        query = query.replace("{measure}",measure);
        String count = handler.selectQuery(query)[0][0];
        return count.equals("0");
    }

    //Inserts a line into the statistic table
    public void insertStats(String measure, String value, String date) {
        try {
            PreparedStatement pstmt = handler.getPreparedStatement("INSERT INTO statistic (date,measure,value) VALUES (?,?,?)");
            pstmt.setDate(1, Date.valueOf(date));
            pstmt.setString(2, measure);
            pstmt.setInt(3, Integer.parseInt(value));

            pstmt.executeUpdate();
            handler.commit();
            pstmt.close();
        } catch (SQLException sqle) {
            statsQueriesLog.error("Error while trying to execute insert query", sqle);
        }
    }

    //Inserts a line into the statistic table using today's date
    public void insertStats(String measure, String value) {
        try {
            PreparedStatement pstmt;
            if (notInsertedToday(measure)) {
                pstmt = handler.getPreparedStatement("INSERT INTO statistic (date,value,measure) VALUES (DATE(NOW()),?,?);");
            } else {
                pstmt = handler.getPreparedStatement("UPDATE statistic SET value = ? WHERE measure = ? AND date = DATE(NOW());");
            }
            pstmt.setInt(1, Integer.parseInt(value));
            pstmt.setString(2, measure);

            pstmt.executeUpdate();

            handler.commit();
            pstmt.close();
        } catch (SQLException sqle) {
            statsQueriesLog.error("Error while trying to execute insert query", sqle);
        }
    }

    public void getRecentData() {
        String[][] indicators = handler.selectQuery("SELECT ref,name FROM ref_indicator;"); //REF | NAME

        String[][] data = new String[indicators.length][3]; //MEASURE | VALUE | DATE

        String[][] singleLine; //VALUE | DATE
        for (int i = 0; i < indicators.length; i++) {
            singleLine = handler.selectQuery("SELECT value,date FROM statistic WHERE measure = '" + indicators[i][0] + "' ORDER BY date DESC LIMIT 1");
            data[i][0] = singleLine[0][1]; //DATE
            data[i][1] = indicators[i][1]; //MEASURE NAME
            data[i][2] = singleLine[0][0]; //VALUE
        }
        encoder.encodeQuery(data, "stats_data");
    }

    public void getHistory(String measure, String interval) {
        String measureRef = handler.selectQuery("SELECT ref FROM ref_indicator WHERE name = '" + measure.replace("'", "''") + "';")[0][0];

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT date,value FROM statistic WHERE ");

        if (!interval.equals("0")){
            queryBuilder.append("date > CURRENT_DATE - INTERVAL '{interval} day' AND ");
        }
        queryBuilder.append("measure = '{measure}' ORDER BY date DESC;");

        String query = queryBuilder.toString()
                .replace("{interval}",interval)
                .replace("{measure}",measureRef);

        String[][] data = handler.selectQuery(query); //DATE | VALUE

        encoder.encodeQuery(data, "stats_history");
    }

    public void getMeasureNames() {
        String[][] data = handler.selectQuery("SELECT name FROM ref_indicator ORDER BY id;");
        // Only has 1 column
        encoder.encodeQuery(data, "stats_measure");
    }

    public void getComparison(String measureX, String measureY) {
        //measureX & measureY are the name of the measure => converting name to ref
        String refX = handler.selectQuery("SELECT ref FROM ref_indicator WHERE name = '" + measureX.replace("'", "''") + "';")[0][0];
        String refY = handler.selectQuery("SELECT ref FROM ref_indicator WHERE name = '" + measureY.replace("'", "''") + "';")[0][0];

        String query = """
            SELECT t.{measureX}, ROUND(AVG(t.{measureY}),1)
            FROM (
                SELECT x.value as {measureX},y.value as {measureY}
                    FROM (
                        SELECT date, measure, value FROM statistic
                        WHERE measure = '{measureX}'
                    ) as x
                JOIN (
                    SELECT date, measure, value FROM statistic
                    WHERE measure = '{measureY}'
                    ) as y
                ON x.date = y.date
                ORDER BY x.value
            ) as t
            GROUP BY t.{measureX};
            """;

        query = query.replace("{measureX}", refX);
        query = query.replace("{measureY}", refY);

        String[][] data = handler.selectQuery(query); //measureX | measureY

        encoder.encodeQuery(data, "stats_comparison");
    }

    public void getComparisonOptions() {
        String query = """
                SELECT x.name as name_x, y.name as name_y FROM(
                	SELECT i.name,c.id FROM ref_indicator as i
                	JOIN ref_comparison as c
                	ON i.ref = c.measure_x
                	) as x  
                JOIN (
                	SELECT i.name,c.id FROM ref_indicator as i
                	JOIN ref_comparison as c
                	ON i.ref = c.measure_y
                	) as y
                ON x.id = y.id;
                """;
        String[][] data = handler.selectQuery(query);
        //ref_comparison contains the ref of each measure
        //the query translates the ref into the names
        encoder.encodeQuery(data, "stats_comparison_options");
    }

    public void updateSensorNumber(){
        String query= """
            SELECT SUM(count) FROM (
                (SELECT COUNT(*) FROM anemometer)
                UNION
                (SELECT count(*) FROM capteur_pluie)
            ) AS t;
        """;
        insertStats("sensor",handler.selectQuery(query)[0][0]);
    }

    public void updateAnemometer(){
        String query = "SELECT COUNT(*) FROM anemometer;";
        insertStats("anemo",handler.selectQuery(query)[0][0]);
    }

    public void updateOtherSensor(){
        String query = "SELECT COUNT(*) FROM capteur_pluie;";
        insertStats("othersens",handler.selectQuery(query)[0][0]);
    }

    public void updatePlot(){
        String query = "SELECT COUNT(*) FROM parcelle;";
        insertStats("plot",handler.selectQuery(query)[0][0]);
    }

    public void updateFarm(){
        String query = "SELECT COUNT(*) FROM utilisation WHERE type_utilisation = 'culture'";
        insertStats("farm",handler.selectQuery(query)[0][0]);
    }

    public void updateBreed(){
        String query = "SELECT COUNT(*) FROM utilisation WHERE type_utilisation = 'elevage'";
        insertStats("breed",handler.selectQuery(query)[0][0]);
    }

    public void updateOffer(){
        String query = "SELECT COUNT(*) FROM offers WHERE date_fin > DATE(NOW());";
        insertStats("offer",handler.selectQuery(query)[0][0]);
    }

    public void updateProduct(){
        String query = "SELECT COUNT(*) FROM product;";
        insertStats("product",handler.selectQuery(query)[0][0]);
    }

    public void updatePanel(){
        String query = "SELECT COUNT(*) FROM panneaux";
        insertStats("panel",handler.selectQuery(query)[0][0]);
    }

    public void updateEnergy(){
        String query = "SELECT 1000*COUNT(*) FROM panneaux";
        insertStats("energy",handler.selectQuery(query)[0][0]);
    }

    public void updateEverything(){
        updateAnemometer();
        updateOtherSensor();
        updateSensorNumber();
        updatePlot();
        updateFarm();
        updateBreed();
        updateOffer();
        updateProduct();
        updatePanel();
        updateEnergy();
    }
}