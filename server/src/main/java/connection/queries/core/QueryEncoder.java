package connection.queries.core;

import org.json.JSONArray;
import org.json.JSONObject;
import server.ThreadClient;

public class QueryEncoder {
	private final ThreadClient client;

    public QueryEncoder(ThreadClient client){
        this.client = client;
    }

	public void encodeQuery(String[][] data,String operation) {
		JSONObject obj = new JSONObject();
		JSONArray list = new JSONArray(data);
		obj.put("data",list);
		obj.put("operation",operation);
		client.sendResponse(obj.toString());
	}
	public void encodeReadProduct(String[][] data_product) {
		JSONObject obj = new JSONObject();
		JSONArray list = new JSONArray(data_product);
		obj.put("data_product",list);
		obj.put("operation","read_product");
		client.sendResponse(obj.toString());
	}
	public void encodeUpdate(int n) {
		JSONObject obj = new JSONObject();
		obj.put("modified_lines", n);
		obj.put("operation","update");
		client.sendResponse(obj.toString());
	}
	public void encodePaneNbr(String[][] data,String operation) {
		JSONObject obj = new JSONObject();
		JSONArray list = new JSONArray(data);
		obj.put("data",list);
		obj.put("operation",operation);
		client.sendResponse(obj.toString());
	}

}