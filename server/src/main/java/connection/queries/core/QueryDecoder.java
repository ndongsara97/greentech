package connection.queries.core;

import connection.queries.business.PanelQueries;
import connection.queries.business.StatsQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class QueryDecoder {
    private final QueryHandler handler;
	private final QueryEncoder encoder;
	private final Logger queryDecoderLog = LogManager.getLogger(QueryHandler.class);
	private final StatsQueries statsQueries;
	private final PanelQueries panelQueries;


    public QueryDecoder(QueryHandler handler, QueryEncoder encoder){
        this.handler = handler;
		this.encoder = encoder;
		statsQueries = new StatsQueries(handler,encoder);
		panelQueries = new PanelQueries(handler, encoder);
    }

    public void decodeOperation(String request) {
		JSONObject json = new JSONObject(request);
		String operation = json.getString("operation");

		switch (operation) {

			//Statistics
			case "stats_data" -> statsQueries.getRecentData();
			case "stats_insert" -> statsQueries.insertStats(json.getString("measure"), json.getString("value"), json.getString("date"));
			case "stats_history" -> statsQueries.getHistory(json.getString("measure"),json.getString("interval"));
			case "stats_measure" -> statsQueries.getMeasureNames();
			case "stats_comparison" -> statsQueries.getComparison(json.getString("measure_x"),json.getString("measure_y"));
			case "stats_comparison_options" -> statsQueries.getComparisonOptions();
			case "stats_update_all" -> statsQueries.updateEverything();
             //Plan ZAHAR
			case "insert_anemometer1" -> handler.insertAnemometer(json.getInt("id_parcelle"),json.getInt("x"),json.getInt("y"),json.getFloat("taille"));
			case "select_anemometer_plot" -> handler.selectDataAnemometerPlot(json.getInt("id_parcelle"));
			case "delete_anemometer" -> handler.deleteAnemometer(json.getInt("id_anemometre"));
			case "select_anemometer_plot_x_y" -> handler.selectDataAnemometerPlot(json.getInt("id_parcelle"),json.getInt("x"),json.getInt("y"));
			case "select_plot" -> handler.selectDataPlots();
			case "insert_sensor" -> handler.insertSensor(json.getInt("id_parcelle"),json.getInt("x"),json.getInt("y"),json.getFloat("taille"));
			case "select_sensor_plot" -> handler.selectDataSensorPlot(json.getInt("id_parcelle"));
			case "select_sensor_plot_x_y" -> handler.selectDataSensorPlot(json.getInt("id_parcelle"),json.getInt("x"),json.getInt("y"));
			case "delete_sensor" -> handler.deleteSensor(json.getInt("id_capteur"));
			case "select_utilisation_plot" -> handler.selectDataUtilisationPlot(json.getInt("id_parcelle"));

			//Anemometer

			case "select_data_anemometer" -> handler.selectDataAnemoemeter();
			case "insert_data_measure" -> handler.insertDataMeasure(json.getDouble("wind_speed"),json.getString("wind_direction"), json.getDouble("temperature"), json.getInt("id_anemometer") );
			case "select_data_measure" -> handler.selectDataMeasure(json.getInt("idAnemometer"));
			case "update_sensor" -> handler.updateSensorOn(json.getInt("id_anemometer"), json.getBoolean("sensorOn"));
			//Commercial offer
			//select for combobox item
			case "select_products" -> handler.readProducts();
			//select for product panel
			case "read_product" -> handler.readProduct();
			case "select_clients" -> handler.readClients();

			case "select_offres" -> handler.offresData();
			case "select_offres_exists" -> handler.selectExitsOffres(json.getString("name_offre"));
			case "delete_offres" -> handler.deleteOffres(json.getString("id"));
			case "delete_product" -> handler.deleteProduct(json.getString("id"));
			case "update_product" -> handler.updateProduct(json.getInt("id"),json.getString("product_name"),json.getInt("price"));
		    case "update_offres" -> handler.updateOffres(json.getInt("id"),json.getString("name_offre"), json.getInt("remise"), json.getString("product_name"),json.getString("type_client"),json.getString("date_debut"),json.getString("date_end"));
			case "insert_offre" -> handler.offreInsert(json.getString("name_offre"), json.getInt("remise"), json.getString("product_name"),json.getString("type_client"),json.getString("date_debut"),json.getString("date_end"));
			case "insert_category" -> handler.insertCategory(json.getString("nomCategory"));
			case "insert_product" -> handler.productInsert(json.getString("product_name"),json.getInt("price"));
			case "select_pane_parcels" -> panelQueries.getPaneParcels();
			case "select_parcels_by_id"  -> panelQueries.selectParcelsById(json.getInt("id_parcelle"), json.getInt("energy"));
			case "insert_panel" -> panelQueries.panelInsert(
					json.getInt("id_panel"),
					json.getInt("x"),
					json.getInt("y"),
					json.getInt("id_parcels"));
			case "setFirstIdPanel" -> panelQueries.setFirstIdPanel(json.getInt("id_panel"));



			default -> queryDecoderLog.error("Unknown operation received : "+ operation);
		}
		statsQueries.updateEverything();
		statsQueries.getRecentData();
	}
}