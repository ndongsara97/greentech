package connection.queries.core;

import connection.pool.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class QueryHandler {
    private Connection connect = null;
    private final Logger queryLog = LogManager.getLogger(QueryHandler.class);
    private final QueryEncoder encoder;

    public QueryHandler(QueryEncoder encoder) {
        this.encoder = encoder;
    }

    public void getConnection() {
        try {
            connect = DataSource.getConnection();
            connect.setAutoCommit(false);
        } catch (SQLException sqle) {
            queryLog.error("Error when trying to retrieve a connection from pool", sqle);
        }
    }

    public void releaseConnection() {
        DataSource.releaseConnection(connect);
        connect = null;
    }

    public String[][] selectQuery(String query) {
        try {
            Statement stmt = connect.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT
            );

            ResultSet rs = stmt.executeQuery(query);

            String[] columnLabels = getColumnLabels(rs);
            int columnNumber = columnLabels.length;
            int rowNumber = getRowNumber(rs);

            if (rowNumber == 0) return null;

            String[][] data = new String[rowNumber][columnNumber];

            int r = 0;
            do {
                for (int c = 0; c < columnNumber; c++) {
                    data[r][c] = rs.getString(c + 1);
                }
                r++;
            } while (rs.next());

            rs.close();
            stmt.close();

            return data;
        } catch (SQLException sqle) {
            queryLog.error("Error when executing select query", sqle);
            return null;
        }
    }

    private String[] getColumnLabels(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();
        String[] columnLabels = new String[columnCount];

        for (int i = 0; i < columnCount; i++) {
            columnLabels[i] = rsmd.getColumnLabel(i + 1); //SQL starts at index 1
        }

        return columnLabels;
    }

    public int getRowNumber(ResultSet rs) throws SQLException {
        rs.last();
        int rowNumber = rs.getRow();
        rs.first();
        return rowNumber;
    }

    public void commit() {
        //Helps identify if an error is related to commiting and not during query execution
        try {
            connect.commit();
        } catch (SQLException sqle) {
            queryLog.error("Error when trying to commit changes to database", sqle);
        }
    }

    public PreparedStatement getPreparedStatement(String query) throws SQLException {
        return connect.prepareStatement(query);
    }

    public void selectDataAnemoemeter() {
        String[][] data = selectQuery("select * from anemometer;");
        encoder.encodeQuery(data, "select_data_anemometer");
    }

    public void selectDataMeasure(int idAnemo) {
        String[][] data = selectQuery("select * from measure where id_anemometer = '" + idAnemo + "' order by id_measure desc limit 1;");
        encoder.encodeQuery(data, "select_data_measure");
    }

    public void updateSensorOn(int id_anemometer, boolean sensorOn) {
        try {
            PreparedStatement pstmt = connect.prepareStatement("UPDATE anemometer SET sensor_on= ? WHERE id_anemometre= ? ;");
            pstmt.setBoolean(1, sensorOn);
            pstmt.setInt(2, id_anemometer);

            int linesChanged = pstmt.executeUpdate();
            pstmt.close();
            commit();
            queryLog.trace("Number of inserted lines : " + linesChanged);
            encoder.encodeUpdate(linesChanged);
        } catch (SQLException sqle) {
            queryLog.error("Error while trying to execute insert query", sqle);
        }
    }

    public void insertDataMeasure(double windSpeed, String windDirection, double temperature, int idAnemometer) {
        try {
            PreparedStatement pstmt = connect.prepareStatement("INSERT INTO measure (id_measure, wind_speed, wind_direction, temperature, id_anemometer) values ((select max(id_measure)+1 from measure),?,?,?,?);");
            pstmt.setDouble(1, windSpeed);
            pstmt.setString(2, windDirection);
            pstmt.setDouble(3, temperature);
            pstmt.setInt(4, idAnemometer);

            pstmt.executeUpdate();
            commit();
            pstmt.close();

        }catch(SQLException sqle) {
            queryLog.error("Error while trying to execute insert query", sqle);
        }
    }

        public void readProducts () {
            String[][] data = selectQuery("SELECT nom_produit FROM product;");
            // Only has 1 column
            encoder.encodeQuery(data, "select_products");
        }

        public void readProduct () {
            String[][] data = selectQuery("SELECT id_produit,nom_produit,prix_init FROM product;");
            // Only has 1 column
            encoder.encodeQuery(data, "read_product");
        }

        public void readClients () {
            String[][] data = selectQuery("SELECT nom_type FROM client_type;");
            // Only has 1 column
            encoder.encodeQuery(data, "select_clients");
        }


        public void insertCategory (String nomCategory){
            try {
                PreparedStatement pstmt = connect.prepareStatement("INSERT INTO  catégorie_produit(nom_catégorie) values(?);");
                pstmt.setString(1, nomCategory);

                int insertedLinesNumber = pstmt.executeUpdate();
                pstmt.close();
                commit();
                queryLog.trace("Number of inserted lines : " + insertedLinesNumber);
            } catch (SQLException sqle) {
                queryLog.error("Error when executing insert update", sqle);
            }
        }

        public void offreInsert (String name_offre, Integer remise, String product_name, String type_client, String
        date_debut, String date_end){
            try {
                String id_produit = selectQuery("select id_produit from product where nom_produit = '" + product_name + "';")[0][0];
                String id_type_client = selectQuery("select id_client from client_type where nom_type = '" + type_client + "';")[0][0];

                PreparedStatement pstmt = connect.prepareStatement("insert into offers (reference,remise,id_produit,id_client,date_debut,date_fin) values (?,?,?,?,?,?)");
                pstmt.setString(1, name_offre);
                pstmt.setInt(2, remise);
                pstmt.setInt(3, Integer.parseInt(id_produit));
                pstmt.setInt(4, Integer.parseInt(id_type_client));
                pstmt.setDate(5, Date.valueOf(date_debut));
                pstmt.setDate(6, Date.valueOf(date_end));
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }


        public void productInsert (String product_name,int price){
            try {
                PreparedStatement pstmt = connect.prepareStatement("INSERT INTO product (nom_produit,prix_init) values (?,?)");
                pstmt.setString(1, product_name);
                pstmt.setInt(2, price);
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
                encoder.encodeUpdate(linesChanged);
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }

        public void offresData () {

            String[][] data = selectQuery("SELECT o.id_offre,o.reference,o.remise,t.nom_type,p.nom_produit,p.prix_init,ROUND(p.prix_init-((p.prix_init*o.remise)/100),2) as price,o.date_debut,o.date_fin  FROM offers as o INNER JOIN product as p  on o.id_produit=p.id_produit INNER JOIN client_type as t on o.id_client=t.id_client;");

            encoder.encodeQuery(data, "select_offres");
        }

        public void selectDataPlots () {
            String[][] data = selectQuery("SELECT * FROM parcelle;");
            encoder.encodeQuery(data, "select_plot");
        }

        public void selectDataAnemometerPlot ( int id_plot){
            String[][] data = selectQuery("SELECT * FROM anemometer where id_parcelle = \'" + id_plot + "\';");
            encoder.encodeQuery(data, "select_anemometer_plot");
        }

        public void selectDataUtilisationPlot ( int id_plot){
            String[][] data = selectQuery("SELECT * FROM utilisation where id_parcelle = \'" + id_plot + "\';");
            encoder.encodeQuery(data, "select_utilisation_plot");
        }

        public void selectDataSensorPlot ( int id_plot){
            String[][] data = selectQuery("SELECT * FROM capteur_pluie where id_parcelle = \'" + id_plot + "\';");
            encoder.encodeQuery(data, "select_sensor_plot");
        }

        public void insertAnemometer ( int id_plot, int x, int y, float capacity){
            try {
                PreparedStatement pstmt = connect.prepareStatement("INSERT INTO anemometer (id_anemometre,id_parcelle,taille,x,y) VALUES (nextval('anemometre_sequence'),?,?,?,?);");
                pstmt.setInt(1, id_plot);
                pstmt.setFloat(2, capacity);
                pstmt.setInt(3, x);
                pstmt.setInt(4, y);
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }

        public void insertSensor ( int id_plot, int x, int y, float capacity){
            try {
                PreparedStatement pstmt = connect.prepareStatement("INSERT INTO capteur_pluie (id_capteur,id_parcelle,taille,x,y) VALUES (nextval('capteur_pluie_sequence'),?,?,?,?);");
                pstmt.setInt(1, id_plot);
                pstmt.setFloat(2, capacity);
                pstmt.setInt(3, x);
                pstmt.setInt(4, y);
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }

        public void selectExitsOffres (String name_offre){
            String[][] data = selectQuery("SELECT count(o.id_offre) as identifiant FROM offers o where reference=  \'" + name_offre + "\' ;");
            // Only has 1 column
            encoder.encodeQuery(data, "select_offres_exists");
        }

        public void deleteOffres (String id){
            try {
                PreparedStatement pstmt = connect.prepareStatement("DELETE FROM offers WHERE id_offre= ? ;");
                pstmt.setInt(1, Integer.parseInt(id));
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }

        public void deleteProduct (String id){
            try {
                PreparedStatement pstmt = connect.prepareStatement("DELETE FROM product WHERE id_produit= ? ;");
                pstmt.setInt(1, Integer.parseInt(id));
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
                encoder.encodeUpdate(linesChanged);
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }

        public void updateOffres (Integer id, String name_offre, Integer remise, String product_name, String
        type_client, String date_debut, String date_end){
            try {
                //getIdProduit
                String id_produit = selectQuery("select id_produit from product where nom_produit = '" + product_name + "';")[0][0];
                //getIdTypeClient
                String id_type_client = selectQuery("select id_client from client_type where nom_type = '" + type_client + "';")[0][0];
                //Querty update
                PreparedStatement pstmt = connect.prepareStatement("UPDATE offers SET remise='" + remise + "',date_debut='" + date_debut + "',date_fin='" + date_end + "',reference='" + name_offre + "',id_client='" + id_type_client + "',id_produit='" + id_produit + "'  WHERE id_offre= ? ;");
                pstmt.setInt(1, id);
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }

        public void selectDataAnemometerPlot ( int id_plot, int x, int y){
            String[][] data = selectQuery("SELECT * FROM anemometer where id_parcelle = \'" + id_plot + "\' and x = \'" + x + "\' and y = \'" + y + "\';");
            encoder.encodeQuery(data, "select_anemometer_plot_x_y");
        }

        public void deleteAnemometer ( int id_anemometer){
            try {
                PreparedStatement pstmt = connect.prepareStatement("DELETE FROM anemometer WHERE id_anemometre = ? ;");
                pstmt.setInt(1, id_anemometer);
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
                encoder.encodeUpdate(linesChanged);
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }

        public void updateProduct (Integer id, String product_name, Integer price){
            try {
                PreparedStatement pstmt = connect.prepareStatement("UPDATE product SET prix_init='" + price + "',nom_produit='" + product_name + "'  WHERE id_produit = ? ;");
                pstmt.setInt(1, id);
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
                encoder.encodeUpdate(linesChanged);
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }

        public void deleteSensor ( int id_sensor){
            try {
                PreparedStatement pstmt = connect.prepareStatement("DELETE FROM capteur_pluie WHERE id_capteur = ? ;");
                pstmt.setInt(1, id_sensor);
                int linesChanged = pstmt.executeUpdate();
                commit();
                pstmt.close();
                encoder.encodeUpdate(linesChanged);
            } catch (SQLException sqle) {
                queryLog.error("Error while trying to execute insert query", sqle);
            }
        }

        public void selectDataSensorPlot ( int id_plot, int x, int y){

            String[][] data = selectQuery("SELECT * FROM capteur_pluie where id_parcelle = \'" + id_plot + "\' and x = \'" + x + "\' and y = \'" + y + "\';");

            encoder.encodeQuery(data, "select_sensor_plot_x_y");
        }


    }
