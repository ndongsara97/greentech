package panels;

public class Pane {

    private static int engSinglePane = 1000;

    private int coordX;
    private int coordY;

    public Pane(int engSinglePane) {
        this.engSinglePane = engSinglePane;
    }

    public int getEngSinglePane() {
        return engSinglePane;
    }

    public void setEngSinglePane(int engSinglePane) {
        this.engSinglePane = engSinglePane;
    }

    public static int calcNbrPane(int totalEnergy){
        int i = totalEnergy/engSinglePane;
        if (totalEnergy%engSinglePane != 0){
            i++;
        }
        return i;
    }

    public int getCoordX() {
        return coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    public static int calcNbPaneBySun (int i) {
        return engSinglePane*i/100;
    }

    public boolean checkCanIPLacaPaneHere(int newX, int newY){
        if (this.getCoordX() == newX && this.getCoordY() == newY){
            return false;
        }
        return true;
    }

}
