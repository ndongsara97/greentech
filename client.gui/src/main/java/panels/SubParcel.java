package panels;

import javax.swing.*;

public class SubParcel extends JPanel {

    private JLabel number = new JLabel();
    float coordX;
    float coordY;

    public SubParcel(int number) {
        super();
        this.number.setText(String.valueOf(number));
        this.add(this.number);
    }

    public JLabel getNumber() {
        return number;
    }

    public String getValue(){
        return number.getText();
    }

    public void setNumber(JLabel number) {
        this.number = number;
    }

    public float getCoordX() {
        return coordX;
    }

    public void setCoordX(float coordX) {
        this.coordX = coordX;
    }

    public float getCoordY() {
        return coordY;
    }

    public void setCoordY(float coordY) {
        this.coordY = coordY;
    }
}
