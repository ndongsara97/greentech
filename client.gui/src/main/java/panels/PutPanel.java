package panels;

import common.gui.MainFrame;
import connection.queries.QueryEncoder;
import jdk.swing.interop.SwingInterOpUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

import static java.lang.Integer.valueOf;


public class PutPanel extends JPanel {
    private JButton placeButton, submitButton;
    private GridLayout gridCenter;
    private JPanel leftPane, rightPane, centerPanel;
    private final String[] plotList = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"};
    private String[] choiceLatLongList;
    private final Logger panelLog = LogManager.getLogger(MainFrame.class);
    private JComboBox<String> choiceComboBox, choiceSubParcelComboBox;
    private JTextField engNeededJTF = new JTextField();
    private JTextField nbrPaneMinJTF = new JTextField();
    private JLabel choicePlotLabel, nbrPaneNeededLabel, nrbPaneToPlaceLabel, placeLabel;
    private ParcelSimulationPanel simu;
    private QueryEncoder encoder;
    private String energy;
    private int idPanel = 0;


    PutPanel(QueryEncoder encoder){

        simu = new ParcelSimulationPanel();
        this.encoder = encoder;
        this.setLayout(new BorderLayout());
        placeButton = new JButton("placer sur la simulation");
        submitButton = new JButton("valider");
        leftPane = new JPanel();
        rightPane = new JPanel();
        centerPanel = new JPanel();
        choicePlotLabel = new JLabel("<html>choisir la parcelle sur laquelle vous souhaitez placer les panneaux :<html/>");
        choicePlotLabel.setHorizontalAlignment(JLabel.CENTER);
        choicePlotLabel.setFont(new Font("Arial", Font.PLAIN,20));

        selectPaneParcels();

        nbrPaneNeededLabel = new JLabel("<html>Voici le nombre de panneaux que vous devez placer :<html/>");
        nbrPaneNeededLabel.setHorizontalAlignment(JLabel.CENTER);
        nbrPaneNeededLabel.setFont(new Font("Arial", Font.PLAIN,20));

        nrbPaneToPlaceLabel = new JLabel("<html>Choisissez l'emplacement où vous voulez placer le panneau dans la parcelle :<html/>");
        nrbPaneToPlaceLabel.setHorizontalAlignment(JLabel.CENTER);
        nrbPaneToPlaceLabel.setFont(new Font("Arial", Font.PLAIN,20));

        placeLabel = new JLabel("<html>Cliquez sur ce bouton pour avoir un apperçu du placement du panneaux dans la parcelle :<html/>");
        placeLabel.setHorizontalAlignment(JLabel.CENTER);
        placeLabel.setFont(new Font("Arial", Font.PLAIN,20));

        choiceComboBox = new JComboBox<>(plotList);
        initchoiceSubParcelComboBox();
        choiceSubParcelComboBox = new JComboBox<>(choiceLatLongList);
        this.add(submitButton, BorderLayout.SOUTH);
        gridCenter = new GridLayout(1,2);
        //ajout des panels dans le panel principal
        this.add(centerPanel, BorderLayout.CENTER);
        centerPanel.setLayout(gridCenter);
        leftPane.setLayout(new BorderLayout());
        centerPanel.add(leftPane, BorderLayout.CENTER);
        centerPanel.add(rightPane, BorderLayout.CENTER);
        rightPane.setLayout(new GridLayout(4,2));
        rightPane.add(choicePlotLabel);

        rightPane.add(choiceComboBox);
        choiceComboBox.setFont(new Font("Arial", Font.PLAIN,20));

        rightPane.add(nbrPaneNeededLabel);

        rightPane.add(nbrPaneMinJTF);
        nbrPaneMinJTF.setHorizontalAlignment(JLabel.CENTER);
        nbrPaneMinJTF.setEditable(false);
        nbrPaneMinJTF.setFont(new Font("Arial", Font.PLAIN,20));

        rightPane.add(nrbPaneToPlaceLabel);
        rightPane.add(choiceSubParcelComboBox);

        rightPane.add(placeLabel);

        rightPane.add(placeButton);

        leftPane.add(simu);

        leftPane.add(engNeededJTF, BorderLayout.SOUTH);
        engNeededJTF.setHorizontalAlignment(JLabel.CENTER);
        engNeededJTF.setFont(new Font("Arial", Font.PLAIN,20));


        rightPane.setBackground(new Color(38, 176, 161));

        placeButton.addActionListener(this::actionPerformed);
        submitButton.addActionListener(this::actionPerformed);
        choiceComboBox.addActionListener(this::actionPerformed);

        //utile pour les fonctions
        //valueOf(nbrPaneToPlaceList[i])
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == placeButton) {
            simu.changeColor(choiceSubParcelComboBox.getSelectedItem().toString());
        }

        if (e.getSource() == submitButton){
            //encoder.setFirstIdPanel();
            //selectParcels();
            idPanel++;
            float x = 0;
            float y = 0;

            for (int i = 0; i< simu.getParcel().length; i++){
                for (int j = 0; j< simu.getParcel().length; j++){
                    if (Integer.valueOf(simu.getParcel(i,j).getValue()) == Integer.valueOf((String) choiceSubParcelComboBox.getSelectedItem())){
                        x = simu.getParcel(i,j).coordX;
                        y = simu.getParcel(i,j).coordY;
                    }
                }
            }

            encoder.insertPanel(idPanel, x, y, Integer.parseInt((String) choiceComboBox.getSelectedItem()));
            //Actualisation energie necessaire
            String tmp = engNeededJTF.getText();
            String[] tab = tmp.split(" : ");
            engNeededJTF.setText("Energie nécessaire : "+String.valueOf(Integer.parseInt(tab[1])-1000));

            //Actualisation nb panneaux
            String value = String.valueOf(Integer.parseInt(nbrPaneMinJTF.getText())-1);
            nbrPaneMinJTF.setText(value);

            JOptionPane.showMessageDialog(null, "<html>Vous avez bien placé un panneaux votre panneau sur la carte, Félicitation !<html/>");
        }
        if (e.getSource() == choiceComboBox){
            int id = Integer.parseInt((String) choiceComboBox.getSelectedItem());
            encoder.selectPlots(id, energy);
        }
    }

    public void selectPaneParcels() {
        encoder.selectOperation("select_pane_parcels");
    }

    public void setParcel(String [][] data){
        String a = "";
        for (String[] tab : data){
            for (String text : tab){
                a = a + " " + text;
            }
        }

        float lat1 = Float.parseFloat(data[0][2]);
        float long1 = Float.parseFloat(data[0][1]);
        float long2 = Float.parseFloat(data[1][1]);
        float lat5 = Float.parseFloat(data[4][2]);
        simu.calcCoordFloat(long1, lat1, long2, lat5);
    }

    public void setPanels(String[][] data){
        // affichage de résultat
        String a = "";
        for (String[] tab : data){
            for (String text : tab){
                a = a + " " + text;
            }
        }
        //---------------------
        String nb = data[0][0];
        if (!nb.equals("0")) {
            nbrPaneMinJTF.setText(nb);
        } else {
            nbrPaneMinJTF.setText(nb);
            JOptionPane.showMessageDialog(this, "Vous ne pouvez pas placer de panneaux sur cette parcelle");
        }
    }

    public void initchoiceSubParcelComboBox(){
        choiceLatLongList = new String[simu.parcelSize()];
        for(int sp = 0; sp<choiceLatLongList.length; sp++){
            choiceLatLongList[sp] = String.valueOf(sp+1);
        }
    }

    public JTextField getEngNeeded() {
        return engNeededJTF;
    }

    public JTextField getNbrPaneMin() {
        return nbrPaneMinJTF;
    }

    public void setEnergy(String energy){
        this.energy = energy;
    }

    public void setFirstIdPanel(String [][] data){
        this.idPanel = Integer.parseInt(data[0][0]);
    }

/*public float CalculCoordFloat(QueryEncoder encoder){
        this.encoder = encoder;
        encoder.selectOperation("select_coord_by_id");
    }*/
}

