package panels;

import connection.queries.QueryEncoder;

import javax.swing.*;
import java.awt.*;

public class ParcelSimulationPanel extends JPanel {

    private static final int X = 20;
    private static final int Y = 20;

    private static SubParcel[][] parcel = new SubParcel[X][Y];

    public ParcelSimulationPanel() {
        this.setLayout(new GridLayout(X, Y));
        panelInit();
    }

    //init Jpanels
    public void panelInit() {
        SubParcel jp;
        int count = 1;
        for (int i = 0; i <= X - 1; i++) {
            for (int j = 0; j <= Y - 1; j++) {
                jp = new SubParcel(count);
                if ((i % 2 == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0)) {
                    jp.setBackground(new Color(17, 112, 57));
                } else {
                    jp.setBackground(new Color(65, 194, 131));
                }
                count++;
                this.add(jp);
                parcel[i][j] = jp;
            }
        }
    }

    public int parcelSize() {
        return X * Y;
    }

    public void changeColor(String k) {
        for (int i = 0; i < parcel.length; i++) {
            for (int j = 0; j < parcel[0].length; j++) {
                if (k.equals(parcel[i][j].getNumber().getText())) {
                    parcel[i][j].setBackground(Color.red);
                }
            }
        }

    }


    public void calcCoordFloat(float x1, float y1, float x2, float y5) {
        float diffLongBetween2Parcel = x2 - x1;
        float longBetween2SubParcel = diffLongBetween2Parcel / parcel.length;
        float longBase = x1 - diffLongBetween2Parcel / 2;
        //longSubParcel = longSubParcel + longBetween2SubParcel;
        float diffLatBetween2Parcel = y5 - y1;
        float latBetween2SubParcel = diffLatBetween2Parcel / parcel.length;
        float latBase = y1 - diffLatBetween2Parcel / 2;
        //latSubParcel = latSubParcel + latBetween2SubParcel;

        for (int i = 0; i < parcel.length; i++) {
            float latSubParcel = latBase;
            float longSubParcel = longBase;
            for (int j = 0; j < parcel.length; j++) {
                longSubParcel = longSubParcel + longBetween2SubParcel;
                if (i == 0 && j == 0) {
                    continue;
                }else if (i == 0){
                    parcel[i][j].setCoordX(parcel[i][j-1].getCoordX() + longSubParcel);
                    parcel[i][j].setCoordY(latBase + latSubParcel);
                }else if (j == 0){
                    parcel[i][j].setCoordX(longBase + longSubParcel);
                    parcel[i][j].setCoordY(parcel[i][j].getCoordY() + latSubParcel);
                }else{
                    parcel[i][j].setCoordX(parcel[i][j - 1].getCoordX() + longSubParcel);
                    parcel[i][j].setCoordY(parcel[i - 1][j].getCoordY() + latSubParcel);
                }
            }
        }
    }

    public SubParcel getParcel(int i, int j) {
        return parcel[i][j];
    }

    public SubParcel[][] getParcel() {
        return parcel;
    }
}
