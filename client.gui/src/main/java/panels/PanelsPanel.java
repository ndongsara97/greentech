package panels;

import common.gui.MainFrame;
import connection.queries.QueryEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class PanelsPanel extends JPanel {
    JPanel centerPanel;
    JButton infoBoutton, putBoutton;
    CardLayout card;
    Color bgColor = new Color(45, 134, 89);
    Color buttonColor = new Color(230, 255, 242);
    QueryEncoder encoder = new QueryEncoder();
    PutPanel place = new PutPanel(encoder);
    InfoPanel info = new InfoPanel(this,place);
    private final Logger putPanelLog = LogManager.getLogger(MainFrame.class);

    public PanelsPanel(){

        JPanel navigationPanel = new JPanel();
        GridLayout grid = new GridLayout(1,2);
        grid.setHgap(50);
        navigationPanel.setLayout(grid);
        navigationPanel.setBorder(new EmptyBorder(10,50,0,50));
        navigationPanel.setBackground(bgColor);

        infoBoutton = new JButton("Informations");
        putBoutton = new JButton("Placer les panneaux");
        resetButtonColor();
        infoBoutton.setBackground(buttonColor); //default selected panel

        navigationPanel.add(infoBoutton);
        navigationPanel.add(putBoutton);

        card = new CardLayout();
        centerPanel = new JPanel(card);
        centerPanel.setBorder(new EmptyBorder(10,20,20,20));
        centerPanel.setBackground(bgColor);

        centerPanel.add("infos", info);
        centerPanel.add("placer", place);
        card.first(centerPanel);

        ActionListener act = e -> {
            if (e.getSource()==infoBoutton){
                resetButtonColor();
                infoBoutton.setBackground(buttonColor);
                card.show(centerPanel,"infos");
            }
            if (e.getSource()==putBoutton){
                resetButtonColor();
                putBoutton.setBackground(buttonColor);
                card.show(centerPanel,"placer");
            }
        };

        infoBoutton.addActionListener(act);
        putBoutton.addActionListener(act);
        this.setLayout(new BorderLayout());
        this.add(navigationPanel,BorderLayout.NORTH);
        this.add(centerPanel,BorderLayout.CENTER);
    }

    public void resetButtonColor(){
        infoBoutton.setBackground(Color.WHITE);
        putBoutton.setBackground(Color.WHITE);
    }

    public void showPut(){
        card.show(centerPanel,"placer");
    }

    public void setPanelsData(String [][] data) {
        place.setPanels(data);
    }

    public void setParcel(String [][] data){
        place.setParcel(data);
    }

    public void setFirstIdPanel(String [][] data){
        place.setFirstIdPanel(data);
    }

}