package panels;

import connection.queries.QueryEncoder;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;

import static java.lang.Integer.valueOf;


public class InfoPanel extends JPanel {
    JButton putButton, submitEnergyNeeded;
    JLabel energyNeededJL, nbrPaneMinText, pressButtonToPutText;
    JTextField engNeededJTF, nbrPaneMinJTF;
    PanelsPanel panelsPanel;
    PutPanel putPanel;
    JPanel paneEnergyNeeded;

    InfoPanel(PanelsPanel panelsPanel, PutPanel put) {
        this.setBackground(new Color(38, 176, 161));
        this.panelsPanel = panelsPanel;
        setBounds(100, 100, 850, 577);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        this.putPanel = put;
        this.setLayout(new GridLayout(3, 2, 0, 0));

        energyNeededJL = new JLabel("<html>Saisir l'énergie nécessaire :<br/><br/>appuyez sur le bouton pour valider<html/>");
        energyNeededJL.setHorizontalAlignment(JLabel.CENTER);
        energyNeededJL.setFont(new Font("Arial", Font.PLAIN, 20));
        this.add(energyNeededJL);

        paneEnergyNeeded = new JPanel();
        paneEnergyNeeded.setLayout(new BorderLayout());
        this.add(paneEnergyNeeded);

        submitEnergyNeeded = new JButton("<html>Valider et Caluler le nombre de panneaux nécessaire<html/>");
        submitEnergyNeeded.addActionListener(this::actionPerformed);
        engNeededJTF = new JTextField();
        engNeededJTF.setHorizontalAlignment(JTextField.CENTER);
        engNeededJTF.setFont(new Font("Arial", Font.PLAIN, 50));
        paneEnergyNeeded.add(engNeededJTF, BorderLayout.CENTER);
        paneEnergyNeeded.add(submitEnergyNeeded, BorderLayout.SOUTH);

        nbrPaneMinText = new JLabel("<html>nombre de panneaux minimal nécéssaire : <html/>");
        nbrPaneMinText.setHorizontalAlignment(JLabel.CENTER);
        nbrPaneMinText.setFont(new Font("Arial", Font.PLAIN, 20));
        this.add(nbrPaneMinText);

        nbrPaneMinJTF = new JTextField();
        nbrPaneMinJTF.setHorizontalAlignment(JTextField.CENTER);
        nbrPaneMinJTF.setFont(new Font("Arial", Font.PLAIN, 50));
        this.add(nbrPaneMinJTF);

        pressButtonToPutText = new JLabel("<html>Appuyez sur le bouton pour placer des panneaux<html/>");
        pressButtonToPutText.setHorizontalAlignment(JLabel.CENTER);
        pressButtonToPutText.setFont(new Font("Arial", Font.PLAIN, 20));
        this.add(pressButtonToPutText);

        putButton = new JButton("<html>Placer les panneaux<html/>");
        putButton.addActionListener(this::actionPerformed);
        this.add(putButton);
    }


    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == putButton) {
            try {
                if (valueOf(engNeededJTF.getText()) < 0) {
                    throw new NumberFormatException();
                } else {
                    putPanel.setEnergy(engNeededJTF.getText());
                    putPanel.getEngNeeded().setText("énergie nécessaire : " + getEngNeeded().getText());
                    //putPanel.getNbrPaneMin().setText(getNbrPaneMin().getText());
                    //putPanel.selectParcels();
                    panelsPanel.showPut();
                }
            } catch (NumberFormatException a) {
                JOptionPane.showMessageDialog(null, "Veillez rentrer un entier positif!");
            }
        }
        if (e.getSource() == submitEnergyNeeded) {
            try {
                if (valueOf(engNeededJTF.getText()) < 0) {
                    throw new NumberFormatException();
                } else {
                    nbrPaneMinJTF.setText(String.valueOf(Pane.calcNbrPane(valueOf(engNeededJTF.getText()))));
                }
            } catch (NumberFormatException a) {
                JOptionPane.showMessageDialog(null, "Veillez rentrer un entier positif!");
            }
        }
    }

    private void setContentPane(JPanel contentPane) {
    }

    public JTextField getEngNeeded() {
        return engNeededJTF;
    }

    public JTextField getNbrPaneMin() {
        return nbrPaneMinJTF;
    }
}
