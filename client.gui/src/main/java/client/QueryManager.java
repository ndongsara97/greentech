package client;

import common.gui.MainFrame;
import connection.queries.QueryDecoder;
import connection.socket.SocketManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import plan.ObjectConnectedDeletedForm;
import plan.PlotMenuGui;
import plan.PlanPanel;

public class QueryManager {
    private final MainFrame gui;
    private final QueryDecoder decoder = new QueryDecoder();
    private final Logger managerLog = LogManager.getLogger(QueryManager.class);

    QueryManager(){
        gui = new MainFrame();
    }

    public void start(){
        //GUI is set to EXIT_ON_CLOSE so this will stop when the GUI gets closed
        while(true){

            String jsonMessage = SocketManager.receiveMessage();

            if (jsonMessage!=null){
                String operation = decoder.getOperation(jsonMessage);
                switch (operation) {
                    case "stats_data" -> gui.setStatsData(decoder.convertDataToArray(jsonMessage,"data"));

                    case "stats_history" -> gui.setStatsHistory(decoder.convertDataToArray(jsonMessage,"data"));

                    case "stats_measure" -> gui.setStatsMeasure(decoder.convertDataToArray(jsonMessage,"data"));
                    case "stats_comparison" -> gui.setStatsComparison(decoder.convertDataToArray(jsonMessage,"data"));
                    case "stats_comparison_options" -> gui.setStatsComparisonOptions(decoder.convertDataToArray(jsonMessage,"data"));

                    case "select_data_anemometer" -> gui.setAnemometerData(decoder.convertDataToArray(jsonMessage,"data"));
                    case "select_data_measure" -> gui.setMeasureData(decoder.convertDataToArray(jsonMessage,"data"));
//                    case "update_sensor_on" -> gui.updateSensorOn(decoder.convertDataToArray(jsonMessage,"data"));

                    case "select_clients" ->gui.setClientData(decoder.convertDataToArray(jsonMessage,"data"));

                    case "select_products" ->gui.setProductDatas(decoder.convertDataToArray(jsonMessage,"data"));
                    case "select_offres" -> gui.setOffresData(decoder.convertDataToArray(jsonMessage,"data"));
                    case "read_product" -> gui.setProductsListings(decoder.convertDataToArray(jsonMessage,"data"));
                    case "select_parcels_by_id" -> gui.setPanelsData(decoder.convertDataToArray(jsonMessage,"data"));
                    case "select_pane_parcels" -> gui.setParcel(decoder.convertDataToArray(jsonMessage,"data"));

                    case "select_plot" -> PlanPanel.displayPlot(decoder.convertDataToArray(jsonMessage,"data"));
                    case "select_anemometer_plot" -> PlotMenuGui.setAnemometers(decoder.convertDataToArrayHandleEmptyJson(jsonMessage,"data"));
                    case "select_utilisation_plot" -> PlotMenuGui.setUtilisation(decoder.convertDataToArrayHandleEmptyJson(jsonMessage,"data"));
                    case "select_sensor_plot" -> PlotMenuGui.setRain_sensors(decoder.convertDataToArrayHandleEmptyJson(jsonMessage,"data"));
                    case "select_anemometer_plot_x_y" -> ObjectConnectedDeletedForm.setToDeleteAnemometer(decoder.convertDataToArrayHandleEmptyJson(jsonMessage,"data"));
                    case "select_sensor_plot_x_y" -> ObjectConnectedDeletedForm.setToDeleteSensor(decoder.convertDataToArrayHandleEmptyJson(jsonMessage,"data"));

                    case "update" -> managerLog.trace("Lines changed");
                    default -> managerLog.error("Unknown operation : "+operation);
                }
            }
        }
    }
}