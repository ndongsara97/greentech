package client;

import common.gui.ConnectionError;
import connection.socket.SocketManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ClientMain {

    public static void main(String[] args){
        Logger clientLog = LogManager.getLogger(ClientMain.class);
        clientLog.info("Client started");
        boolean connected = new SocketManager().startConnection("172.31.249.43",6666);
        if (connected) {
            new QueryManager().start();
        } else {
            new ConnectionError();
        }
    }
}