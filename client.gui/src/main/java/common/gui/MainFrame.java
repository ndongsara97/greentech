package common.gui;

import anemometre.AnemometerPanel;
import anemometre.Configuration;
import anemometre.ValuesInsert;
import panels.PanelsPanel;
import plan.PlanPanel;
import stats.panel.StatsPanel;
import commercial.Home;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    StatsPanel statsPanel = new StatsPanel();
    Home home = new Home();
    PanelsPanel panelsPanel = new PanelsPanel();
    AnemometerPanel anePan = new AnemometerPanel();


//    Configuration conf = new Configuration(anePan);

    public MainFrame(){

        JTabbedPane tabbedPane = new JTabbedPane();

        tabbedPane.add("Statistiques", statsPanel);
        tabbedPane.add("Plan", new PlanPanel());
        tabbedPane.add("Panneaux", panelsPanel);
        tabbedPane.add("Offres commerciales",home);
        tabbedPane.add("Anémomètres", anePan);
        this.add(tabbedPane, BorderLayout.CENTER);

        this.setBackground(Color.BLACK);
        this.pack();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Greenwatch");
        this.setMinimumSize(new Dimension(1000,500));

    }

    public void setStatsData(String[][] data) {
        statsPanel.setStatsData(data);
    }

    public void setStatsHistory(String[][] data) {
        statsPanel.setStatsHistory(data);
    }

    public void setStatsMeasure(String[][] data) {
        statsPanel.setStatsMeasure(data);
    }

    public void setStatsComparison(String[][] data) {
        statsPanel.setStatsComparison(data);
    }

    public void setStatsComparisonOptions(String[][] data) {
        statsPanel.setStatsComparisonOptions(data);
    }

    public void setClientData(String[][] data) {home.setClientData(data);}



    //select product to combobox items
    public void setProductDatas(String[][] data) {home.setProductDatas(data);}
    //select product to operation read product

    public void setProductsListings(String[][] data) {home.setProductsListings(data);}


    public void setOffresData(String[][] data) {home.setOffresData(data);}


    public void setPanelsData(String[][] data) {
        panelsPanel.setPanelsData(data);
    }
    public void setFirstIdPanel(String [][] data){panelsPanel.setFirstIdPanel(data);}
    public void setParcel(String [][] data){
        panelsPanel.setParcel(data);
    }

    public void setAnemometerData(String[][] data) {
        anePan.getListAnemometer(data);
    }
//    public void updateSensorOn(String[][] data) {
//        conf.updateSensorOn(data);
//    }

    public void setMeasureData(String[][] data){
        anePan.setMeasureData(data);
    }
}
