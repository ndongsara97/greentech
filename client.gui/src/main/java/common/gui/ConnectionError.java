package common.gui;

import javax.swing.*;

public class ConnectionError {

    public ConnectionError(){
        JOptionPane.showMessageDialog(null,
                "Impossible de se connecter au server\nRéessayez plus tard",
                "ERREUR",
                JOptionPane.ERROR_MESSAGE);
    }
}
