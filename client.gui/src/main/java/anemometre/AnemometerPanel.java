package anemometre;

import connection.queries.QueryEncoder;

import javax.swing.*;
import javax.swing.border.Border;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

public class AnemometerPanel extends JPanel {
	private final Color GreenColor = new Color(45, 134, 89);
	public List<Anemometer> listAnemometer;
	public List<String> stringListAnemometer;
	int lastGust = 0;
	private QueryEncoder encoder = new QueryEncoder();
	private Anemometer selectedAnemometer;
	private ValuesInsert vi = new ValuesInsert(this);

	//label of values (which change according to the anemometer selected)
	JLabel labelAlight;
	JLabel labelWindSpeed;
	JLabel labelWindDirection;
	JLabel labelTemperature;
	JLabel labelSize;
	JLabel labelPosX;
	JLabel labelPosY;
	JLabel labelLastGust;

	public String[] getNameAnemometers() {
		List<String> listeNoms = new ArrayList<String>();
		for (int i = 0; i < listAnemometer.size(); i++) 
			listeNoms.add(listAnemometer.get(i).name);
		return listeNoms.toArray(new String[0]);
	}

	public void refreshValues() {

		if(selectedAnemometer.sensorOn) labelAlight.setText("allume");
		else labelAlight.setText("eteint");
		labelWindSpeed.setText(selectedAnemometer.getWindSpeed());
		labelWindDirection.setText(selectedAnemometer.getWindDirection());
		labelTemperature.setText(selectedAnemometer.getTemperature());
		labelSize.setText(selectedAnemometer.getSize());
		labelPosX.setText(selectedAnemometer.getPosX());
		labelPosY.setText(selectedAnemometer.getPosY());
		labelLastGust.setText(String.valueOf(Integer.parseInt(String.valueOf(lastGust))));
	}

	public void getListAnemometer(String[][] data) {
		stringListAnemometer = new ArrayList<String>();
		listAnemometer = new ArrayList<Anemometer>();
		for (int i = 0; i < data.length; i++) {
			Anemometer a = new Anemometer(data[i]);

			listAnemometer.add(a);
			stringListAnemometer.add(a.getName());
		}
		setPanel();
		this.selectedAnemometer = listAnemometer.get(0);
	}

	public AnemometerPanel() {
		encoder.selectOperation("select_data_anemometer");
		//receiveDataSelectAnemometer();
	}
	public void setPanel(){

		setBackground(GreenColor);
		labelAlight = new JLabel("");
		labelWindSpeed = new JLabel("");
		labelWindDirection = new JLabel("");
		labelTemperature = new JLabel("");
		labelSize = new JLabel("");
		labelPosX = new JLabel("");
		labelPosY = new JLabel("");
		labelLastGust = new JLabel("");

		//Creation combobox folding anemometer
		//JComboBox dropDownList = new JComboBox(getnamesAnemometres());
		JComboBox dropDownList = new JComboBox(stringListAnemometer.toArray());

		int index = Integer.parseInt(dropDownList.getSelectedItem().toString().split(" ")[1]);
		for (int i = 0; i < stringListAnemometer.size()-1; i++) {
			if (listAnemometer.get(i).getId() == index-1) {
				selectedAnemometer = listAnemometer.get(index-1);
			}
		}

		//JLabel j1 = new JLabel(""+numAnemometer);

		//drop-down list and refresh action of values
		dropDownList.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int index = Integer.parseInt(dropDownList.getSelectedItem().toString().split(" ")[1]);
				for (int i = 0; i < stringListAnemometer.size()-1; i++) {
					if (listAnemometer.get(i).getId() == index-1) {
						selectedAnemometer = listAnemometer.get(index);
					}
				}
				refreshValues();
			}
		});

		//buttons
		JButton boutonConfiguration = new JButton("Configuration");
		boutonConfiguration.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				callConfig();
			}
		});

		JButton buttonRefresh = new JButton("Actualiser");
		buttonRefresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				vi.addValues();
				receiveDataSelectMeasure();
			}
		});


		//margin management
		int sizeMargins = 20;
		Border margins = BorderFactory.createEmptyBorder(sizeMargins, sizeMargins, sizeMargins, sizeMargins);

		//arrangement of elements: a 4x4 grid
		GridLayout grid = new GridLayout(4, 5, 30, 35);

		//panel which contains the elements
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(margins);
		panel.setLayout(grid);

		//adding elements to the panel
		//line 1
		panel.add(new JLabel("Nom de l'Anemometre"));
		panel.add(dropDownList);
		panel.add(labelAlight);
		panel.add(boutonConfiguration);
		panel.add(buttonRefresh);

		//line 2
		panel.add(new JLabel("Vitesse du vent"));
		panel.add(labelWindSpeed);
		panel.add(new JLabel("Direction du vent"));
		panel.add(labelWindDirection);
		panel.add(new JLabel("Dernière Rafale"));

		//line 3
		panel.add(new JLabel("Temperature"));
		panel.add(labelTemperature);
		panel.add(new JLabel("Taille"));
		panel.add(labelSize);
		panel.add(labelLastGust);

		//line 4
		panel.add(new JLabel("Position"));
		panel.add(labelPosX);
		panel.add(labelPosY);
		panel.add(new JLabel(""));

		this.add(panel);
	}

	public void receiveDataSelectMeasure(){
		encoder.selectAnemometer("select_data_measure", selectedAnemometer.getId());
	}

	public void insertDataMeasuresIntoAnemometer(double windSpeed, String windDirection, double temperature, int idAnemometer){
		encoder.insertDataMeasures(windSpeed, windDirection, temperature, idAnemometer);
	}

	public void callConfig() {
		new Configuration(this);
		this.setVisible(false);
	}

	public void setMeasureData(String[][] data){
		this.selectedAnemometer.setWindSpeed(Double.parseDouble(data[0][1]));
		this.selectedAnemometer.setWindDirection(data[0][2]);
		this.selectedAnemometer.setTemperature(Double.parseDouble(data[0][3]));
		this.selectedAnemometer.setLastGust(Integer.parseInt(data[0][4]));

		refreshValues();
	}

	public Anemometer getSelectedAnemometer() {
		return selectedAnemometer;
	}

	public void setSelectedAnemometer(Anemometer selectedAnemometer) {
		this.selectedAnemometer = selectedAnemometer;
	}
}
