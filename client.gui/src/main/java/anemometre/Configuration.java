package anemometre;

import connection.queries.QueryEncoder;

import javax.swing.*;
import javax.swing.border.Border;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;


public class Configuration extends JFrame {
	private QueryEncoder encoder = new QueryEncoder();

	public Configuration(AnemometerPanel fen){

		this.setTitle("Configuration");
		this.setLocationRelativeTo(null);

		Anemometer selectionAnemometer = fen.getSelectedAnemometer();


		//elements : buttons
		JRadioButton yesButton = new JRadioButton("Oui");
		yesButton.setActionCommand("Oui");
		JRadioButton noButton = new JRadioButton("Non");
		noButton.setActionCommand("Non");

		if(selectionAnemometer.sensorOn)
			yesButton.setSelected(true);
		else
			noButton.setSelected(true);

		ButtonGroup group = new ButtonGroup();
		group.add(yesButton);
		group.add(noButton);

		JButton buttonValidate = new JButton("Valider");
		buttonValidate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				encoder.selectOperation("select_data_anemometer");
				//
				//if(group.getSelection().getActionCommand() != null)
				if(group.getSelection().getActionCommand().equals("Oui")) {
					selectionAnemometer.sensorOn = true;
					//TODO remplacer par une requete pour un boolean
					updateSensorOn(fen.getSelectedAnemometer().getId(), fen.getSelectedAnemometer().isSensorOn());
				}
				else {
					selectionAnemometer.sensorOn = false;
					updateSensorOn(fen.getSelectedAnemometer().getId(), fen.getSelectedAnemometer().isSensorOn());
				}

				fen.refreshValues();
				fen.setVisible(true);
				dispose();
			}
		});
		JButton buttonCancel = new JButton("Annuler");
		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fen.setVisible(true);
				dispose();
			}
		});

		//margin management
		int sizeMargins = 20;
		Border margins = BorderFactory.createEmptyBorder(sizeMargins, sizeMargins, sizeMargins, sizeMargins);

		//arrangement of elements: a 4x4 grid
		GridLayout grid = new GridLayout(4, 2, 30, 20);

		//panel which contains the elements
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(margins);
		panel.setLayout(grid);


		//adding elements to the panel
		//line 1
		panel.add(new JLabel(selectionAnemometer.name));
		panel.add(new JLabel(""));

		//line 2
		panel.add(new JLabel("Capteur allume"));
		panel.add(new JLabel(""));

		//line 3
		panel.add(yesButton);
		panel.add(noButton);

		//line 4
		panel.add(buttonValidate);
		panel.add(buttonCancel);

		//in case of closing the window :
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				fen.setVisible(true);	//remettre visible la fen�tre principal
				dispose();				//fermer cette fen�tre
			}
		});

		this.add(panel);
		this.pack();
		this.setVisible(true);
	}

	public void updateSensorOn(int id_anemo, boolean sensorOn){
		encoder.updateSensor(id_anemo, sensorOn);
	}
}
