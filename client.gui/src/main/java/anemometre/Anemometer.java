package anemometre;

import java.util.ArrayList;
import java.util.List;

public class Anemometer {
	protected int id;
	protected String name;
	protected double windSpeed;
	protected String windDirection;
	protected double temperature;
	protected double size;
	protected double posX;
	protected double posY;
	protected boolean sensorOn = false;
	protected int lastGust;

	public Anemometer() {

	}

	public int getLastGust() {
		return lastGust;
	}

	public void setLastGust(int lastGust) {
		this.lastGust = lastGust;
	}

	static String defaultValue = null;

	List<Double> windSpeedHisto = new ArrayList<Double>();
	List<String> windDirectionHisto = new ArrayList<String>();


	public Anemometer(String[] param){
		//id_anemometre | id_parcelle | taille |  x  |  y
		id = Integer.parseInt(param[0]);
		size = Double.parseDouble(param[2]);
		posX = Double.parseDouble(param[3]);
		posY = Double.parseDouble(param[4]);
		name = "Anemometre " + id;
	}

	public Anemometer(int id, String name) {
		this.id = id;
		this.name = name;
		windSpeed = id*10+10;
	}

	@Override
	public String toString() {
		return "Anemometer{" +
				"id=" + id +
				", name='" + name + '\'' +
				", windSpeed=" + windSpeed +
				", windDirection='" + windDirection + '\'' +
				", temperature=" + temperature +
				", size=" + size +
				", posX=" + posX +
				", posY=" + posY +
				", sensorOn=" + sensorOn +
				", windSpeedHisto=" + windSpeedHisto +
				", windDirectionHisto=" + windDirectionHisto +
				'}';
	}

	public String getWindSpeed() {
			return windSpeed+"";
	}
	
	public void setWindSpeed(double windSpeed) {
		this.windSpeed = windSpeed;		//update the value
		windSpeedHisto.add(windSpeed);	//add it to the history
		if(windSpeedHisto.size()>10)	//if history contains + 10 values
			windSpeedHisto.remove(0);	//we delete the 1er value (the most old)
	}

	public String getWindDirection() {
			return windDirection+"";
	}
	
	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;		//update the value
		windDirectionHisto.add(windDirection);	//add it to the history
		if(windDirectionHisto.size()>10)		//if history contains + 10 values
			windDirectionHisto.remove(0);	//we delete the 1er value (the most old)
	}

	public String getTemperature() {
			return temperature+"";
	}

	public String getSize() {
			return size+"";
	}

	public String getPosX() {
			return posX+"";
	}

	public String getPosY() {
			return posY+"";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public boolean isSensorOn() {
		return sensorOn;
	}

	public void setSensorOn(boolean sensorOn) {
		this.sensorOn = sensorOn;
	}

	public static String getDefaultValue() {
		return defaultValue;
	}

	public static void setDefaultValue(String defaultValue) {
		Anemometer.defaultValue = defaultValue;
	}

	public List<Double> getWindSpeedHisto() {
		return windSpeedHisto;
	}

	public void setWindSpeedHisto(List<Double> windSpeedHisto) {
		this.windSpeedHisto = windSpeedHisto;
	}

	public List<String> getWindDirectionHisto() {
		return windDirectionHisto;
	}

	public void setWindDirectionHisto(List<String> windDirectionHisto) {
		this.windDirectionHisto = windDirectionHisto;
	}
}
