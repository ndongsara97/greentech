package anemometre;

import java.util.Random;

public class ValuesInsert{

    AnemometerPanel ap;
    Random rdm = new Random();
    String[] direction = {"NORTH", "SUD", "WEST", "EST"};
    Anemometer anemo = new Anemometer();

    public ValuesInsert(AnemometerPanel anemoP) {
        ap = anemoP;
    }

    public void addValues() {
        //while (ap.isDisplayable()) {
            for (int i = 0; i < ap.listAnemometer.size()-1; i++) {
                try {
                    if (ap.getSelectedAnemometer().sensorOn) {
                        anemo.setId(i+1);
                        anemo.setWindSpeed(((double) Math.round(Math.random() * 100)) / 10);
                        anemo.setWindDirection(direction[rdm.nextInt(3)]);
                        anemo.temperature = ((double) Math.round(Math.random() * 100)) / 10;

                        //TODO voir si cette méthode marche

                        if (anemo.getWindSpeed() != null) {

                            ap.insertDataMeasuresIntoAnemometer(
                                    Double.parseDouble(anemo.getWindSpeed()),
                                    anemo.getWindDirection(),
                                    Double.parseDouble(anemo.getTemperature()),
                                    anemo.getId()
                            );
                        }


                    }
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
    }
}
