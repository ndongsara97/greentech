package stats.chart;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.DefaultTableXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;

public class HistoryChart extends JPanel {
    ChartPanel chartPanel;

    public HistoryChart() {
        this.setLayout(new BorderLayout());

        JFreeChart lineChart = ChartFactory.createTimeSeriesChart(
                "Vide",
                "Date",
                "Valeur",
                new DefaultTableXYDataset(),
                false,false,false);

        chartPanel = new ChartPanel( lineChart );
        chartPanel.setMaximumDrawHeight(Integer.MAX_VALUE); //Prevents the font from resizing
        chartPanel.setMaximumDrawWidth(Integer.MAX_VALUE);
        this.add(chartPanel , BorderLayout.CENTER);
    }

    public void setHistoryChart(String measure, String[][] data){
        JFreeChart timeSeriesChart = ChartFactory.createTimeSeriesChart(
                "Historique pour : "+measure,
                "Date",
                measure,
                getTimeDataSet(data, measure),
                false,
                true,
                true
        );
        timeSeriesChart.setBackgroundPaint(new Color(102, 183, 147));
        timeSeriesChart.getXYPlot().getRangeAxis().setTickLabelFont(new Font("Arial", Font.PLAIN, 13));
        timeSeriesChart.getXYPlot().getDomainAxis().setTickLabelFont(new Font("Arial", Font.PLAIN, 13));
        timeSeriesChart.getXYPlot().getRangeAxis().setLabelFont(new Font("Arial", Font.PLAIN, 20));
        timeSeriesChart.getXYPlot().getDomainAxis().setLabelFont(new Font("Arial", Font.PLAIN, 20));
        chartPanel.setChart(timeSeriesChart);
    }

    public XYDataset getTimeDataSet(String[][] data, String measure){
        TimeSeriesCollection dataSet = new TimeSeriesCollection();

        TimeSeries series = new TimeSeries(measure);
        for (int i = 0; i < data.length; i++) {
            series.add(getDay(data[i][0]),Integer.parseInt(data[i][1]));
        }
        dataSet.addSeries(series);
        return dataSet;
    }

    public Day getDay(String date){
        //Date = YYYY-MM-DD
        int year = Integer.parseInt(date.substring(0,4));
        int month = Integer.parseInt(date.substring(5,7));
        int day = Integer.parseInt(date.substring(8,10));
        return new Day(day,month,year);
    }
}