package stats.chart;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;

public class ComparisonChart extends JPanel {

    ChartPanel chartPanel;

    public ComparisonChart() {
        this.setLayout(new BorderLayout());

        JFreeChart lineChart = ChartFactory.createLineChart(
                "Vide",
                "Date",
                "Valeur",
                new DefaultCategoryDataset(),
                PlotOrientation.VERTICAL,
                true,true,false);

        chartPanel = new ChartPanel( lineChart );
        chartPanel.setMaximumDrawHeight(Integer.MAX_VALUE); //Prevents the font from resizing
        chartPanel.setMaximumDrawWidth(Integer.MAX_VALUE);
        this.add(chartPanel, BorderLayout.CENTER);
    }
    public void setComparisonChart(String nameX, String nameY, String[][] data){
        JFreeChart chart = ChartFactory.createXYLineChart(
                nameY+" en fonction de "+nameX,
                nameX,
                nameY,
                getLineDataSet(data),
                PlotOrientation.VERTICAL ,
                true , true , false
        );
        chartPanel.setChart(chart);
    }

    private XYDataset getLineDataSet(String[][] data) {
        XYSeries xySeries = new XYSeries("Valeur");

        for (int i = 0; i < data.length; i++) {
            xySeries.add(Double.parseDouble(data[i][0]),Double.parseDouble(data[i][1]));
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(xySeries);

        return dataset;
    }
}
