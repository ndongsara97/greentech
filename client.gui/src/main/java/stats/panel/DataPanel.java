package stats.panel;

import connection.queries.QueryEncoder;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;

public class DataPanel extends JPanel {
    private final String[] columnNames = {"Date","Mesure","Valeur"};
    private final Color bgColor = new Color(45, 134, 89);
    private final JTable dataTable = new JTable();
    private final QueryEncoder encoder;

    public DataPanel(QueryEncoder encoder){
        this.encoder = encoder;

        JButton updateButton = new JButton("Mettre à jour les données");
        updateButton.addActionListener(e -> {
            int input = JOptionPane.showConfirmDialog(null,
                    "Êtes-vous sûr de vouloir mettre à jour toutes les données ?",
                    "Confirmation de la mise à jour",
                    JOptionPane.YES_NO_OPTION);
            if (input == 0){
                encoder.selectOperation("stats_update_all");
                refreshData();
            }
        });
        JButton refreshButton = new JButton("Actualiser");
        refreshButton.addActionListener(e -> refreshData());
        JPanel southPanel = new JPanel();
        southPanel.add(refreshButton);
        southPanel.add(updateButton);
        southPanel.setBackground(bgColor);

        this.setLayout(new BorderLayout());
        this.add(new JScrollPane(dataTable),BorderLayout.CENTER);
        this.add(southPanel,BorderLayout.SOUTH);
        this.setBackground(bgColor);

        refreshData();
    }

    private void refreshData() {
        encoder.selectOperation("stats_data");
    }

    public void setData(String[][] data){
        dataTable.setModel(new DefaultTableModel(data,columnNames){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        });

        dataTable.getColumnModel().getColumn(0).setMinWidth(100);
        dataTable.getColumnModel().getColumn(0).setMaxWidth(100);
        dataTable.getColumnModel().getColumn(1).setMinWidth(300);
        dataTable.getColumnModel().getColumn(1).setMaxWidth(300);
        dataTable.setIntercellSpacing(new Dimension(20,0));
        dataTable.setRowHeight(20);
    }
}
