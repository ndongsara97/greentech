package stats.panel;

import common.gui.MainFrame;
import connection.queries.QueryEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class StatsPanel extends JPanel {
    private final JButton dataButton, historyButton, comparisonButton;
    private final CardLayout card;
    private final Logger statsLog = LogManager.getLogger(MainFrame.class);
    private final Color bgColor = new Color(45, 134, 89);
    private final Color buttonColor = new Color(230, 255, 242);

    private final QueryEncoder encoder = new QueryEncoder();
    private final DataPanel dataPanel = new DataPanel(encoder);
    private final HistoryPanel historyPanel = new HistoryPanel(encoder);
    private final ComparisonPanel comparisonPanel = new ComparisonPanel(encoder);


    public StatsPanel(){

        JPanel navigationPanel = new JPanel();
        GridLayout grid = new GridLayout(1,3);
        grid.setHgap(50);
        navigationPanel.setLayout(grid);
        navigationPanel.setBorder(new EmptyBorder(10,50,0,50));
        navigationPanel.setBackground(bgColor);

        dataButton = new JButton("Données");
        historyButton = new JButton("Historique");
        comparisonButton = new JButton("Comparaison");
        resetButtonColor();
        dataButton.setBackground(buttonColor); //default selected panel

        navigationPanel.add(dataButton);
        navigationPanel.add(historyButton);
        navigationPanel.add(comparisonButton);

        card = new CardLayout();
        JPanel centerPanel = new JPanel(card);
        centerPanel.setBorder(new EmptyBorder(10,20,20,20));
        centerPanel.setBackground(bgColor);

        centerPanel.add("data", dataPanel);
        centerPanel.add("history", historyPanel);
        centerPanel.add("comparison", comparisonPanel);
        card.first(centerPanel);

        ActionListener act = e -> {
            if (e.getSource()==dataButton){
                resetButtonColor();
                dataButton.setBackground(buttonColor);
                card.show(centerPanel,"data");
            }
            if (e.getSource()==historyButton){
                resetButtonColor();
                historyButton.setBackground(buttonColor);
                card.show(centerPanel,"history");
            }
            if (e.getSource()==comparisonButton){
                resetButtonColor();
                comparisonButton.setBackground(buttonColor);
                card.show(centerPanel,"comparison");

            }
        };

        dataButton.addActionListener(act);
        historyButton.addActionListener(act);
        comparisonButton.addActionListener(act);

        this.setLayout(new BorderLayout());
        this.add(navigationPanel,BorderLayout.NORTH);
        this.add(centerPanel,BorderLayout.CENTER);
    }

    public void resetButtonColor(){
        dataButton.setBackground(Color.WHITE);
        historyButton.setBackground(Color.WHITE);
        comparisonButton.setBackground(Color.WHITE);
    }

    public void setStatsData(String[][] data) {
        dataPanel.setData(data);
    }

    public void setStatsHistory(String[][] data){
        historyPanel.setData(data);
    }

    public void setStatsMeasure(String[][] data) {
        historyPanel.setMeasure(data);
    }

    public void setStatsComparison(String[][] data) {
        comparisonPanel.setData(data);
    }

    public void setStatsComparisonOptions(String[][] data) {
        comparisonPanel.setComparisonOptions(data);
    }
}
