package stats.panel;

import connection.queries.QueryEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import stats.chart.ComparisonChart;
import stats.chart.HistoryChart;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class ComparisonPanel extends JPanel {
    private final Logger comparisonLog = LogManager.getLogger(HistoryPanel.class);
    private final JTable dataTable = new JTable();
    private final Color bgColor = new Color(45, 134, 89);
    private final JComboBox<String> comparisonOptions;
    private final ComparisonChart chart;
    private String[][] comparisonList;
    private final QueryEncoder encoder;

    public ComparisonPanel(QueryEncoder encoder){
        this.encoder = encoder;

        comparisonOptions = new JComboBox<>();
        comparisonOptions.addActionListener(e -> {
            String[] options = getSelectedOptions();
            encoder.selectStatsComparison(options[0],options[1]);
        });

        JPanel measurePanel = new JPanel();
        measurePanel.setBorder(new EmptyBorder(0,0,10,0));
        measurePanel.add(comparisonOptions);
        measurePanel.setBackground(bgColor);

        chart = new ComparisonChart();

        JPanel centerPanel = new JPanel(new GridLayout(1,2));
        centerPanel.add(new JScrollPane(dataTable));
        centerPanel.add(chart);

        this.setLayout(new BorderLayout());
        this.add(measurePanel, BorderLayout.NORTH);
        this.add(centerPanel,BorderLayout.CENTER);

        encoder.selectOperation("stats_comparison_options");
    }

    public void setData(String[][] data){
        String[] options = getSelectedOptions();
        dataTable.setModel(new DefaultTableModel(data, options) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        dataTable.getColumnModel().getColumn(0).setMaxWidth(300);
        dataTable.getColumnModel().getColumn(1).setMaxWidth(300);
        dataTable.setIntercellSpacing(new Dimension(20,0));
        dataTable.setRowHeight(20);

        chart.setComparisonChart(
                options[0],
                options[1],
                data
        );
    }

    public void setComparisonOptions(String[][] data) {
        setComparisonList(data);

        String[] options = new String[data.length];
        for (int i = 0; i < data.length; i++) {
            options[i] = data[i][1] + " en fonction de "+ data[i][0];
        }
        comparisonOptions.setModel(new DefaultComboBoxModel<>(options));

        encoder.selectStatsComparison(data[0][0],data[0][1]);
    }

    public void setComparisonList(String[][] data){
        comparisonList = data;
    }

    public String[] getSelectedOptions(){
        int index = comparisonOptions.getSelectedIndex();
        return new String[]{comparisonList[index][0], comparisonList[index][1]};
    }
}