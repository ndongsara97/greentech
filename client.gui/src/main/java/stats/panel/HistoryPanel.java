package stats.panel;

import connection.queries.QueryEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import stats.chart.HistoryChart;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class HistoryPanel extends JPanel {
    private final Logger historyLog = LogManager.getLogger(HistoryPanel.class);
    private final String[] columnNames = {"Date","Valeur"};
    private final JTable dataTable = new JTable();
    private final Color bgColor = new Color(45, 134, 89);
    private final JComboBox<String> measureOptions,timeOptions;
    private final HistoryChart chart;
    private final QueryEncoder encoder;

    public HistoryPanel(QueryEncoder encoder){
        this.encoder = encoder;

        measureOptions = new JComboBox<>();
        encoder.selectOperation("stats_measure");

        String[] timeList = {"Tout les temps",
                "Ce mois",
                "Ces 3 derniers mois",
                "Cette année"};
        timeOptions = new JComboBox<>(timeList);

        ActionListener act = e -> {
            String measure = (String) measureOptions.getSelectedItem();
            String t = (String) timeOptions.getSelectedItem();

            getHistory(measure,t);
        };

        measureOptions.addActionListener(act);
        timeOptions.addActionListener(act);

        JLabel histLab = new JLabel("MESURE", SwingConstants.LEFT);
        histLab.setForeground(Color.WHITE);
        JLabel interLab = new JLabel("HISTORIQUE DATANT DE", SwingConstants.LEFT);
        interLab.setForeground(Color.WHITE);

        JPanel topOptionsPanel = new JPanel();
        GridLayout grid = new GridLayout(2,2);
        grid.setHgap(100);

        topOptionsPanel.setLayout(grid);
        topOptionsPanel.add(histLab);
        topOptionsPanel.add(interLab);
        topOptionsPanel.add(measureOptions);
        topOptionsPanel.add(timeOptions);
        topOptionsPanel.setBorder(new EmptyBorder(0,100,10,100));
        topOptionsPanel.setBackground(bgColor);

        chart = new HistoryChart();

        JPanel centerPanel = new JPanel(new GridLayout(1,2));
        centerPanel.add(new JScrollPane(dataTable));
        centerPanel.add(chart);

        this.setLayout(new BorderLayout());
        this.add(topOptionsPanel, BorderLayout.NORTH);
        this.add(centerPanel,BorderLayout.CENTER);

    }

    private void getHistory(String measure, String t) {
        String timeInterval;
        switch(t){
            case "Ce mois" -> timeInterval = "30";
            case "Ces 3 derniers mois" -> timeInterval = "90";
            case "Cette année" -> timeInterval = "365";
            default -> timeInterval = "0";
        }

        encoder.selectStatsHistory(measure,timeInterval);
    }

    public void setData(String[][] data) {
        dataTable.setModel(new DefaultTableModel(data, columnNames) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        dataTable.getColumnModel().getColumn(0).setMaxWidth(100);
        dataTable.getColumnModel().getColumn(0).setMinWidth(100);
        dataTable.setIntercellSpacing(new Dimension(20,0));
        dataTable.setRowHeight(20);

        chart.setHistoryChart((String) measureOptions.getSelectedItem(),data);
    }

    public void setMeasure(String[][] data) {
        String[] options = new String[data.length];
        for (int i = 0; i < data.length; i++) {
            options[i] = data[i][0];
        }
        measureOptions.setModel(new DefaultComboBoxModel<>(options));
        getHistory((String) measureOptions.getSelectedItem(),(String) timeOptions.getSelectedItem());
    }
}