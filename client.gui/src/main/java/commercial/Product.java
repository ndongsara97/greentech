
package commercial;

        import connection.queries.QueryEncoder;

        import javax.swing.table.DefaultTableModel;
        import javax.swing.table.TableModel;
        import javax.swing.*;
        import javax.swing.border.EmptyBorder;

        import java.awt.*;
        import java.awt.event.ActionEvent;
        import java.awt.event.ActionListener;
        import java.awt.event.MouseAdapter;
        import java.awt.event.MouseEvent;
        import java.text.SimpleDateFormat;

        import java.text.DateFormat;

        import java.util.Locale;

        import static util.CheckFormat.isInt;


public class Product extends JPanel {

    private final String[] columnNames = {"#","Nom ", "prix",};
    private final JTable dataTable = new JTable();
    private final Color bgColor = new Color(45, 134, 89);
    private JTextField product_name;
    private JTextField price;
    private QueryEncoder encoder;

    private JButton btnSave;
    private JButton btnUpdate;

    public Product(QueryEncoder encoder) {

        UIManager.put("OptionPane.noButtonText", "Non");

        UIManager.put("OptionPane.yesButtonText", "Oui");
        this.encoder = encoder;
        encoder.selectOperation("read_product");

        this.product_name = new JTextField();
        this.price = new JTextField();
        this.btnSave = new JButton("Ajouter");

        this.btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Product.this.btnSaveActionPerformed(e);
            }
        });

        this.btnUpdate = new JButton("Modifer");
        btnUpdate.setText("Modifier");

        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });




        JLabel nameLab = new JLabel("Nom", SwingConstants.LEFT);
        nameLab.setBounds(40, 50, 150, 150);
        nameLab.setForeground(Color.WHITE);

        JLabel priceLab = new JLabel("Prix", SwingConstants.LEFT);
        priceLab.setForeground(Color.WHITE);

        JPanel topOptionsPanel = new JPanel();
        GridLayout grid = new GridLayout(2, 2);
        grid.setHgap(100);

        JPanel buttonPanel = new JPanel();
        GridLayout gridButton = new GridLayout(2, 2);
        gridButton.setHgap(100);


        topOptionsPanel.setLayout(grid);
        topOptionsPanel.add(nameLab);
        topOptionsPanel.add(priceLab);
        topOptionsPanel.add(product_name);
        topOptionsPanel.add(price);

        topOptionsPanel.setBorder(new EmptyBorder(0, 100, 20, 100));
        topOptionsPanel.setBackground(bgColor);

        buttonPanel.add(btnSave);
        buttonPanel.add(btnUpdate);

        buttonPanel.setBorder(new EmptyBorder(0, 100, 20, 100));
        buttonPanel.setBackground(bgColor);
        dataTable.setCellSelectionEnabled(true);
        dataTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                btnSave.setEnabled(false);
                int i = dataTable.getSelectedRow();
                TableModel model = dataTable.getModel();
                product_name.setText(model.getValueAt(i, 1).toString());
                price.setText(model.getValueAt(i, 2).toString());
            }
        });

        this.setLayout(new BorderLayout());
        this.add(topOptionsPanel, BorderLayout.NORTH);
        this.add(buttonPanel, BorderLayout.SOUTH);
        this.dataTable.setRowHeight(30);
        this.dataTable.setRowHeight(2,50);
        this.dataTable.setFillsViewportHeight(true);
        this.add(new JScrollPane(dataTable), BorderLayout.CENTER);


    }


    public void setData(String[][] data) {
        dataTable.setModel(new DefaultTableModel(data, columnNames) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        dataTable.getColumnModel().getColumn(0).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(1).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(2).setMaxWidth(200);

    }



    private void refreshData() {

        encoder.selectOperation("read_product");
    }


    private void btnSaveActionPerformed(ActionEvent event) {
        String product_name = this.product_name.getText().trim();
        String price = this.price.getText().trim();
        if (!product_name.isEmpty() && !price.isEmpty() ) {
            if (isInt(price)) {
                encoder.insertProduct(product_name, price);
                refreshData();
                clear();
                JOptionPane.showMessageDialog(this, "enregisté", "Information", 3);

            }else{
                JOptionPane.showMessageDialog(this, "Erreur : prix doit être un entier", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        }
        else {
            JOptionPane.showMessageDialog(this, "veuillez remplir tous les champs", "Error", 1);
        }


    }

    //handles update button action

    private void btnUpdateActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        int i = dataTable.getSelectedRow();
        if (i >= 0) {
            TableModel model = dataTable.getModel();
            String id = model.getValueAt(i, 0).toString();
            String product_name = this.product_name.getText().trim();
            String price = this.price.getText().trim();
            if (!id.isEmpty() && !product_name.isEmpty() && !price.isEmpty() ) {
            int option = JOptionPane.showConfirmDialog(getRootPane(),
                    " Etes-vous sûr de vouloir modifier?", "Modification la confirmation", JOptionPane.YES_NO_OPTION);
                if (option == 0) {
                    encoder.updateProduct(id, product_name, price);
                    refreshData();
                    clear();
                    btnSave.setEnabled(true);
                    alert("La mise à jour du produit réussi");
                }
            } else {
                alert("Veuillez remplir tous les champs");
            }
        } else {
            alert("Veuillez sélectionner une ligne à modifier");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed


    //method to clear the txt fields
    private void clear() {
        product_name.setText("");
        price.setText("");
    }

    //method to show an info alert
    public void alert(String msg) {
        JOptionPane.showMessageDialog(getRootPane(), msg);
    }



}



