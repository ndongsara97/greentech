package commercial;

import common.gui.MainFrame;
import connection.queries.QueryEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class Home extends JPanel {

    JButton  productButton ,panelOffresButton;
    CardLayout card;
    Logger offresLog = LogManager.getLogger(MainFrame.class);
    Color bgColor = new Color(45, 134, 89);
    Color buttonColor = new Color(230, 255, 242);

    private final QueryEncoder encoder = new QueryEncoder();

    private final Product product = new Product(encoder);
    private final OffresPanel offresPanel = new OffresPanel(encoder);

    public Home() {

        JPanel navigationPanel = new JPanel();
        GridLayout grid = new GridLayout(1,3);
        grid.setHgap(50);
        navigationPanel.setLayout(grid);
        navigationPanel.setBorder(new EmptyBorder(10,50,0,50));
        navigationPanel.setBackground(bgColor);

        panelOffresButton = new JButton("Offres");

        productButton = new JButton("Produits");


        resetButtonColor();
        panelOffresButton.setBackground(buttonColor); //default selected panel

        navigationPanel.add(panelOffresButton);

        navigationPanel.add(productButton);


        card = new CardLayout();
        JPanel centerPanel = new JPanel(card);
        centerPanel.setBorder(new EmptyBorder(10,20,20,20));
        centerPanel.setBackground(bgColor);

        centerPanel.add("offre", offresPanel);

        centerPanel.add("product", product);


        card.first(centerPanel);

        ActionListener act = e -> {
            if (e.getSource()== panelOffresButton){
                encoder.selectOperation("select_products");
                resetButtonColor();
                panelOffresButton.setBackground(buttonColor);
                card.show(centerPanel,"offre");
            }


            if (e.getSource()==productButton){
                resetButtonColor();
                productButton.setBackground(buttonColor);
                card.show(centerPanel,"product");

            }

        };

        panelOffresButton.addActionListener(act);

        productButton.addActionListener(act);



        this.setLayout(new BorderLayout());
        this.add(navigationPanel,BorderLayout.NORTH);
        this.add(centerPanel,BorderLayout.CENTER);
    }

    public void resetButtonColor(){
        panelOffresButton.setBackground(Color.WHITE);

        productButton.setBackground(Color.WHITE);


    }


    public void setClientData(String[][] data) {
        offresPanel.setClient(data);
    }



    public void setProductDatas(String[][] data) {offresPanel.setProduct(data);
    }

    public void setProductsListings(String[][] data) {product.setData(data);
    }



    public void setOffresData(String[][] data) {
        offresPanel.setData(data);
    }


}









 
        