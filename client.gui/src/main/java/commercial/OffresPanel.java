package commercial;

import common.gui.MainFrame;
import connection.queries.QueryEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import connection.queries.QueryEncoder;
import org.jdatepicker.DateModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import util.CheckFormat;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DateFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import java.text.DateFormat;
import java.text.Format;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;

import static util.CheckFormat.isInt;


public class OffresPanel extends JPanel {

    private final String[] columnNames = {"#", "Nom offres", "Remise", "Client", "Produit (1Kg)", "Prix Initial", "Prix remise", "Date Début", "Date fin"};
    private final JTable dataTable = new JTable();
    private final Color bgColor = new Color(45, 134, 89);
    private final JComboBox<String> clientOptions;
    private final JComboBox<String> productOptions;
    private JTextField name_offre;
    private JTextField remise_offre;
    private QueryEncoder encoder;
    private JButton btnDelete1;
    private JButton btnSave;
    ;
    private JButton btnUpdate;

    private JDatePanelImpl date_start;
    private JDatePanelImpl date_end;
    private JDatePickerImpl datePicker;
    private JDatePickerImpl datePickers;

    Logger offresLog = LogManager.getLogger(OffresPanel.class);


    public OffresPanel(QueryEncoder encoder) {

        clientOptions = new JComboBox<>();
        this.encoder = encoder;
        productOptions = new JComboBox<>();

        encoder.selectOperation("select_products");
        encoder.selectOperation("select_clients");
        encoder.selectOperation("select_offres");


        this.name_offre = new JTextField();
        this.remise_offre = new JTextField();
        this.btnSave = new JButton("Ajouter");


        this.btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OffresPanel.this.btnSaveActionPerformed(e);
            }
        });


        this.btnUpdate = new JButton("Modifer");
        btnUpdate.setText("Modifier");

        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        this.btnDelete1 = new JButton("Supprimer");
        btnDelete1.setText("Supprimer");

        btnDelete1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDelete1ActionPerformed(evt);
            }
        });

        UtilDateModel modelDateStart = new UtilDateModel();
        Properties pStart = new Properties();
        pStart.put("text.today", "Today");
        pStart.put("text.month", "Month");
        pStart.put("text.year", "Year");
        JDatePanelImpl datePanelStart = new JDatePanelImpl(modelDateStart, pStart);
        datePickers = new JDatePickerImpl(datePanelStart, new DateLabelFormatter());

        UtilDateModel modelDateEnd = new UtilDateModel();
        Properties pEnd = new Properties();
        pEnd.put("text.today", "Today");
        pEnd.put("text.month", "Month");
        pEnd.put("text.year", "Year");

        JDatePanelImpl datePanelEnd = new JDatePanelImpl(modelDateEnd, pEnd);

        datePicker = new JDatePickerImpl(datePanelEnd, new DateLabelFormatter());

        JLabel productLab = new JLabel("Produit (1kg)", SwingConstants.LEFT);
        productLab.setForeground(Color.WHITE);
        JLabel typeLab = new JLabel("Type client", SwingConstants.LEFT);
        typeLab.setForeground(Color.WHITE);

        JLabel nameLab = new JLabel("Nom", SwingConstants.LEFT);
        nameLab.setBounds(40, 50, 150, 150);
        nameLab.setForeground(Color.WHITE);

        JLabel remiseLab = new JLabel("Remise", SwingConstants.LEFT);
        remiseLab.setForeground(Color.WHITE);

        JPanel topOptionsPanel = new JPanel();
        GridLayout grid = new GridLayout(2, 2);
        grid.setHgap(100);

        JLabel dateStartLab = new JLabel("Date début", SwingConstants.LEFT);
        dateStartLab.setForeground(Color.WHITE);

        JLabel ldate_end = new JLabel("Date fin", SwingConstants.LEFT);
        ldate_end.setForeground(Color.WHITE);


        JPanel buttonPanel = new JPanel();
        GridLayout gridButton = new GridLayout(2, 2);
        gridButton.setHgap(100);


        topOptionsPanel.setLayout(grid);
        topOptionsPanel.add(productLab);
        topOptionsPanel.add(typeLab);
        topOptionsPanel.add(nameLab);
        topOptionsPanel.add(remiseLab);
        topOptionsPanel.add(dateStartLab);
        topOptionsPanel.add(ldate_end);
        topOptionsPanel.add(productOptions);
        topOptionsPanel.add(clientOptions);
        topOptionsPanel.add(name_offre);
        topOptionsPanel.add(remise_offre);
        topOptionsPanel.add(datePickers);
        topOptionsPanel.add(datePicker);
        topOptionsPanel.setBorder(new EmptyBorder(0, 100, 20, 100));
        topOptionsPanel.setBackground(bgColor);

        buttonPanel.add(btnSave);

        buttonPanel.add(btnUpdate);
        buttonPanel.add(btnDelete1);

        buttonPanel.setBorder(new EmptyBorder(0, 100, 20, 100));
        buttonPanel.setBackground(bgColor);
        dataTable.setCellSelectionEnabled(true);
        dataTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                btnSave.setEnabled(false);
                int i = dataTable.getSelectedRow();
                TableModel model = dataTable.getModel();
                name_offre.setText(model.getValueAt(i, 1).toString());
                remise_offre.setText(model.getValueAt(i, 2).toString());

                clientOptions.getSelectedItem();
            }

        });

        productOptions.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }
        });

        this.setLayout(new BorderLayout());
        this.add(topOptionsPanel, BorderLayout.NORTH);
        this.add(buttonPanel, BorderLayout.SOUTH);
        this.add(new JScrollPane(dataTable), BorderLayout.CENTER);

    }


    //set data from select offer in datatable
    public void setData(String[][] data) {
        dataTable.setModel(new DefaultTableModel(data, columnNames) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        dataTable.getColumnModel().getColumn(0).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(1).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(2).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(3).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(4).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(5).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(6).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(7).setMaxWidth(200);
        dataTable.getColumnModel().getColumn(8).setMaxWidth(200);
    }

    //set product in jcombobox productoption
    public void setProduct(String[][] data) {
        String[] options = new String[data.length];
        for (int i = 0; i < data.length; i++) {
            options[i] = data[i][0];



        }

        productOptions.setModel(new DefaultComboBoxModel<>(options));

    }




    //set product in jcombobox clientoption
    public void setClient(String[][] data) {
        String[] options = new String[data.length];
        for (int i = 0; i < data.length; i++) {
            options[i] = data[i][0];
        }

        clientOptions.setModel(new DefaultComboBoxModel<>(options));

    }


    //methode to refresh data automatically
    private void refreshData() {

        encoder.selectOperation("select_offres");
    }


    private void btnSaveActionPerformed(ActionEvent event) {

        DateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);

        int i = dataTable.getSelectedRow();
        if (this.datePickers.getModel().getValue() != null && this.datePicker.getModel().getValue() != null) {
            String name_offre = this.name_offre.getText().trim();
            String remise = this.remise_offre.getText().trim();
            String product_name = this.productOptions.getSelectedItem().toString();
            String type_client = this.clientOptions.getSelectedItem().toString();
            Date selected_date_start = (Date) this.datePickers.getModel().getValue();
            Date selected_date_end = (Date) this.datePicker.getModel().getValue();
            String date_debut = dateFormat.format(selected_date_start);
            String date_end = dateFormat.format(selected_date_end);

            if (!name_offre.isEmpty() && !remise.isEmpty() && !type_client.isEmpty() && !product_name.isEmpty() && date_debut != null && date_end != null) {
                if (isInt(remise)) {
                    try {
                        //encoder.selectExitsOffres(name_offre);
                        encoder.insertOffre(name_offre, remise, product_name, type_client, date_debut, date_end);
                        refreshData();
                        clear();
                        JOptionPane.showMessageDialog(this, "lignes insérées", "Information", 3);
                    } catch (Exception error) {
                        error.printStackTrace();
                        JOptionPane.showMessageDialog(this, "Problème survenu lors de la création de l'offre");
                        //offresLog.error(OffresPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Erreur : Remise doit être un entier", "Erreur", JOptionPane.ERROR_MESSAGE);

                }

            } else {
                JOptionPane.showMessageDialog(this, "veuillez remplir tous les champs", "Error", 1);
            }
        } else {
            JOptionPane.showMessageDialog(this, "veuillez remplir tous les champs", "Error", 1);
        }

    }


    //handles delete button action
    private void btnDelete1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelete1ActionPerformed
        // TODO add your handling code here:

        int i = dataTable.getSelectedRow();
        if (i >= 0) {
            int option = JOptionPane.showConfirmDialog(getRootPane(),
                    "Vous êtes sûr de vouloir supprimer?", "Supprimer la confirmation", JOptionPane.YES_NO_OPTION);
            if (option == 0) {
                TableModel model = dataTable.getModel();
                String id = model.getValueAt(i, 0).toString();

                if (dataTable.getSelectedRows().length == 1) {
                    encoder.deleteOffres(id);
                    DefaultTableModel model1 = (DefaultTableModel) dataTable.getModel();
                    model1.setRowCount(0);
                    clear();
                    refreshData();
                    btnSave.setEnabled(true);
                }
            }
        } else {
            alert("Veuillez sélectionner une ligne à supprimer");
        }
    }


    private void btnUpdateActionPerformed(ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        DateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);

        int i = dataTable.getSelectedRow();
        if (i >= 0) {
            TableModel model = dataTable.getModel();
            String id = model.getValueAt(i, 0).toString();

            if (this.datePickers.getModel().getValue() != null && this.datePicker.getModel().getValue() != null) {
                String name_offre = this.name_offre.getText().trim();
                String remise = this.remise_offre.getText().trim();
                String product_name = this.productOptions.getSelectedItem().toString();
                String type_client = this.clientOptions.getSelectedItem().toString();
                Date selected_date_start = (Date) this.datePickers.getModel().getValue();
                Date selected_date_end = (Date) this.datePicker.getModel().getValue();
                String date_debut = dateFormat.format(selected_date_start);
                String date_end = dateFormat.format(selected_date_end);

                if (!id.isEmpty() && !name_offre.isEmpty() && !remise.isEmpty() && !type_client.isEmpty() && !product_name.isEmpty() && !date_debut.isEmpty() && !date_end.isEmpty()) {
                    if (isInt(remise)) {
                        int option = JOptionPane.showConfirmDialog(getRootPane(),
                                "Vous êtes sûr de vouloir modifier?", "Modification la confirmation", JOptionPane.YES_NO_OPTION);
                        if (option == 0) {
                            encoder.updateOffres(id, name_offre, remise, product_name, type_client, date_debut, date_end);
                            refreshData();
                            clear();
                            btnSave.setEnabled(true);
                            alert("La mise à jour a réussi");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Erreur : Remise doit être un entier", "Erreur", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    alert("Il n'y a pas de telle offre pour mofifier");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Veuillez remplir tous les champs", "Error", 1);
            }
        } else {
            alert("Veuillez sélectionner une ligne à modifier");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed


    //method to clear the txt fields
    private void clear() {
        name_offre.setText("");
        remise_offre.setText("");
    }

    //method to show an info alert
    public void alert(String msg) {
        JOptionPane.showMessageDialog(getRootPane(), msg);
    }


}



