package plan;

import javax.swing.*;
import java.awt.*;

public class PlotCanvasKeys extends JPanel {
    public PlotCanvasKeys() {
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - zahar jihane
        label1 = new JLabel();
        label2 = new JLabel();

        //======== this ========
        setMinimumSize(new Dimension(200, 250));
        setMaximumSize(new Dimension(200, 250));
        setPreferredSize(new Dimension(200, 250));
        setBorder(new javax.swing.border.CompoundBorder(new javax.swing.border.TitledBorder(new javax.swing
        .border.EmptyBorder(0,0,0,0), "JF\u006frmD\u0065sig\u006eer \u0045val\u0075ati\u006fn",javax.swing.border.TitledBorder
        .CENTER,javax.swing.border.TitledBorder.BOTTOM,new java.awt.Font("Dia\u006cog",java.
        awt.Font.BOLD,12),java.awt.Color.red), getBorder()))
        ; addPropertyChangeListener(new java.beans.PropertyChangeListener(){@Override public void propertyChange(java.beans.PropertyChangeEvent e
        ){if("\u0062ord\u0065r".equals(e.getPropertyName()))throw new RuntimeException();}})
        ;

        //---- label1 ----
        label1.setText("Capteur de pluie");
        label1.setIcon(new ImageIcon(System.getenv("greenwatch")+"/etc/icons8-iot-64 (1).png"));

        //---- label2 ----

        label2.setText(" Anemometre");

        label2.setIcon(new ImageIcon(System.getenv("greenwatch")+"/etc/icons8-wind-64 (1).png"));


        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addGap(42, 42, 42)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(label2, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(24, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addGap(74, 74, 74)
                    .addComponent(label1)
                    .addGap(29, 29, 29)
                    .addComponent(label2)
                    .addContainerGap(92, Short.MAX_VALUE))
        );

    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - zahar jihane
    private JLabel label1;
    private JLabel label2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
