package plan;

import entity.Anemometer;
import entity.RainSensor;
import entity.Plot;
import entity.Utilisation;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UtilisationGui extends JFrame implements ActionListener {

    public UtilisationGui(Utilisation utilisation, Plot plot, Anemometer[] anemometers, RainSensor[] rain_sensors) throws HeadlessException {

        JPanel  resultPanel = new JPanel(new BorderLayout());
        JPanel  northPanel = new JPanel(new BorderLayout());
        PlotCanvas plotCanvas = new PlotCanvas(anemometers , rain_sensors);
//        PlotCanvas plotCanvas = new PlotCanvas(anemometers , rain_sensors, panels);
        plotCanvas.setPreferredSize(new Dimension(550, 250));
        northPanel.add(plotCanvas,BorderLayout.CENTER);
        northPanel.add(new PlotCanvasKeys(),BorderLayout.EAST);
        JTextArea column1 = new JTextArea();
        column1.setText(
                "Les informations de la parcelle : " +
                "\n-type d'utilisation : " + utilisation.getType_utilisation()+
                "\n-nom : " + utilisation.getName()+
                "\n-latitude : " + plot.getLatitude()+
                        "\n-longitude : " + plot.getLongitude()+
                        "\n-taux d'ensoleillenent : " + plot.getSunshine_rate()+
                        "\n-taux d'humidité : " + plot.getHumidity_level()+
                        "\n-largeur : " + plot.getWidth()+
                        "\n-longueur : " + plot.getLenght()+
                        "\n-superficie : " + plot.getArea()+
                        "\n-nombre total d'anémomètres : " + anemometers.length+
                        "\n-nombre total de capteurs de pluie : " + rain_sensors.length+
                        "\n-nombre total d'objets connectés : " + (rain_sensors.length+ anemometers.length)
        );

        column1.setEditable(false);
        Font font = column1.getFont();
        column1.setFont( font.deriveFont(24f));
//        column1.setPreferredSize(new Dimension( 750,2000));
//        JPanel test = new JPanel();
//        JScrollPane scrollFrame = new JScrollPane(test);
//        test.setAutoscrolls(true);
//        test.add(column1);
        resultPanel.add(column1,BorderLayout.CENTER);

        this.add(resultPanel,BorderLayout.CENTER);
        this.add(northPanel,BorderLayout.NORTH);
        this.setTitle("Information sur la parcelle");
        this.pack();
        this.setSize(750,800);
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
