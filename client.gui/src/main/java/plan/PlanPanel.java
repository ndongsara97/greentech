package plan;

import entity.Plot;
import service.PlotService;

import javax.swing.*;
import java.awt.*;

public class PlanPanel extends JPanel {
    public static PlotService plotService;
    public  static JPanel buttonPanel;
    private static Plot[] plots;
    public PlanPanel()  {
        plotService = new PlotService();
        plotService.sendSelect();
        this.setLayout(new BorderLayout());
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(4,4));

//        displayParcelle(parcelles,buttonPanel);

        //title of the UI
        JTextArea text= new JTextArea("                                                                   Voici votre plan",1,10);
        this.add(text,BorderLayout.BEFORE_FIRST_LINE);
        this.add(buttonPanel,BorderLayout.CENTER);
        text.setEnabled(false);
        text.setFont(new Font("Serif", Font.BOLD, 30));
        text.setForeground(Color.blue);
    }

    public  static void displayPlot(String [][] plots_s ){
        plots = plotService.getAllPlot(plots_s);
        for (Plot plot: plots) {
            JButton mybutton = new JButton(plot.getBasicInfo());
            mybutton.addActionListener(e -> {
                new PlotMenuGui(plot);
            });
            mybutton.setContentAreaFilled(false); // We set to false to prevent the component from painting the inside of the JButton
            mybutton.setFocusPainted(false); //The focus effect is not displayed.
            buttonPanel.add(mybutton);
        }
    }

}
