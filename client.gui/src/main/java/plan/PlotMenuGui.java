package plan;

import entity.Anemometer;
import entity.RainSensor;
import entity.Plot;
import entity.Utilisation;
import service.AnemometerService;
import service.SensorService;
import service.PlotService;
import service.UtilisationService;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlotMenuGui extends JFrame implements ActionListener {

    public static Utilisation utilisation;
    public static  Anemometer[] anemometers;
    public static   RainSensor[] rain_sensors;
//    public static   Panel[] panels;
    private static Plot plot;

    CardLayout card = new CardLayout();

    public static void refreshData(){
        //get Utilisation
        UtilisationService utilisationService = new UtilisationService();
        SensorService sensorService = new SensorService();
        //get Anemometre
        AnemometerService anemometerService = new AnemometerService();
        utilisationService.sendSelectUtilisationByPlotId(plot.getId_plot());
        //get Sensorss

        sensorService.sendSelectSensors(plot.getId_plot());
        anemometerService.sendSelectAnemometers(plot.getId_plot());

        //get Panneaux

//        sensorService.sendSelectSensors(plot.getId_plot());
//        anemometerService.sendSelectAnemometers(plot.getId_plot());
    }

    public static void setPanneaux(String[][] panneaux) {

    }
    public static void setUtilisation(String[][] utilisations) {
        UtilisationService utilisationService = new UtilisationService();
        PlotMenuGui.utilisation = utilisationService.getUtilisation(utilisations);
    }

    public static void setAnemometers(String[][] anemometers) {
        AnemometerService anemometerService = new AnemometerService();
        PlotMenuGui.anemometers = anemometerService.getAnemometers(anemometers);
    }

    public static void setRain_sensors(String[][] rain_sensors) {
        SensorService capteurService = new SensorService();
        PlotMenuGui.rain_sensors = capteurService.getSensors(rain_sensors);
    }
    public PlotMenuGui(Plot plot1){
        plot = plot1;
        UtilisationService utilisationService = new UtilisationService();
        SensorService sensorService = new SensorService();
        AnemometerService anemometerService = new AnemometerService();
        utilisationService.sendSelectUtilisationByPlotId(plot.getId_plot());
        sensorService.sendSelectSensors(plot.getId_plot());
        anemometerService.sendSelectAnemometers(plot.getId_plot());

        JPanel menuPanel = new JPanel();
        JPanel loadingPanel = new JPanel();
        loadingPanel.setLayout(new BorderLayout());

        //Creating buttons
        JButton detailsButton = new JButton("Plus de détails");
        detailsButton.setContentAreaFilled(false);
        detailsButton.setFocusPainted(false);
        detailsButton.addActionListener(e -> {
            new UtilisationGui(utilisation,plot,anemometers,rain_sensors);
//            new UtilisationGui(utilisation,plot,anemometers,rain_sensors,panels);
        });
        JButton  insertButton = new JButton("Ajouter un objet connecté à cette parcelle");
        insertButton.setContentAreaFilled(false);
        insertButton.setFocusPainted(false);
        insertButton.addActionListener(e->{
            //new InsertAnemometreGui(parcelle);
            if((rain_sensors.length + anemometers.length) < PlotService.MAXCONNECTEDOBJECTS){
                try {
                    new ObjectConnectedForm(anemometers,rain_sensors,plot.getId_plot());
                }catch (Exception e1){
                    e1.printStackTrace();
                }
            }else{
                JOptionPane.showMessageDialog(this,
                        "Vous avez atteint le nombre maximum d'objets connectés ( MAX = " + PlotService.MAXCONNECTEDOBJECTS +")", "Erreur de capacité ",
                        JOptionPane.ERROR_MESSAGE);
            }
        });


        JButton  deleteButton = new JButton("Supprimer un objet connecté de cette parcelle");
        deleteButton.setContentAreaFilled(false);
        deleteButton.setFocusPainted(false);
        deleteButton.addActionListener(e->{
            new ObjectConnectedDeletedForm(plot.getId_plot(),this);
        });
        //Adding buttons to the main menu panel
        menuPanel.setLayout(new GridLayout(3, 1));
        menuPanel.add(detailsButton);
        menuPanel.add(insertButton);
       // menuPanel.add(deleteButton);

        JPanel mainPanel = new JPanel(card);

        mainPanel.add("menu", menuPanel);
        card.show(mainPanel, "menu");
        this.add(mainPanel, BorderLayout.CENTER);
        this.pack();
        this.setSize(500, 300);
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }
    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
