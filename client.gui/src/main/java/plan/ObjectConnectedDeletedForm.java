/*
 * Created by JFormDesigner on Mon May 09 21:30:48 CEST 2022
 */

package plan;

import client.QueryManager;
import entity.Anemometer;
import entity.RainSensor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.AnemometerService;
import service.SensorService;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
public class ObjectConnectedDeletedForm extends JFrame {
    private int plot_id ;
    private static PlotMenuGui parcelleMenuGui;
    private static int nb=0;
    private static boolean show = false;
    private static final Logger managerLog = LogManager.getLogger(ObjectConnectedDeletedForm.class);

    public static void setPlotMenuGui(PlotMenuGui parcelleMenuGui) {
        ObjectConnectedDeletedForm.parcelleMenuGui = parcelleMenuGui;
    }
    public ObjectConnectedDeletedForm(int plot_id, PlotMenuGui parcelleMenuGui) {
        initComponents();
        this.setVisible(true);
       this.plot_id = plot_id;
        ObjectConnectedDeletedForm.parcelleMenuGui= parcelleMenuGui;
    }

    public static void setToDeleteAnemometer(String[][] data) {
        AnemometerService anemometerService = new AnemometerService();
        Anemometer[] anemometers = anemometerService.getAnemometers(data);
        if(anemometers.length==0 && nb>0 && !show){
            managerLog.info("hello form delete object");
            JOptionPane.showMessageDialog(parcelleMenuGui,
                    "il n'existe aucun objet dans cette position. ");
        }else if(anemometers.length>0)  {
            for (Anemometer anemometer: anemometers) {
                AnemometerService anemometerService1 = new AnemometerService();
                anemometerService1.sendDeleteAnemometers(anemometer);
            }
            show=true;
                JOptionPane.showMessageDialog(parcelleMenuGui,
                        "Objet Connecté dans la position (x,y)=("+anemometers[0].getX()+","+anemometers[0].getY()+") à été supprimé avec succès");
        }

        if(nb==0)nb++;
        else {nb=0;show=false;}
    }

    public static void setToDeleteSensor(String[][] data) {
        SensorService sensorService = new SensorService();
        RainSensor[] rain_sensors = sensorService.getSensors(data);
        if(rain_sensors.length==0 && nb>0 && !show){
            JOptionPane.showMessageDialog(parcelleMenuGui,
                    "il n'existe aucun objet dans cette position. ");
        }else if(rain_sensors.length>0) {
            for (RainSensor sensor: rain_sensors) {
                sensorService.sendDeleteSensors(sensor);
            }
            show=true;
                JOptionPane.showMessageDialog(parcelleMenuGui,
                        "Objet Connecté dans la position (x,y)=("+rain_sensors[0].getX()+","+rain_sensors[0].getY()+") a été supprimé avec succès");
        }
        if(nb==0)nb++;
        else {nb=0;show=false;}
    }

    private void delete(ActionEvent e) {
        SensorService sensorService=new SensorService();
        sensorService.sendSelectSensorsXY(plot_id,Integer.parseInt(x.getText()),Integer.parseInt(y.getText()));

        AnemometerService anemometerService = new AnemometerService();
        anemometerService.sendSelectAnemometersXY(plot_id,Integer.parseInt(x.getText()),Integer.parseInt(y.getText()));
    }

    private void supprimer(ActionEvent e) {
        // TODO add your code here
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - zahar jihane
        x = new JTextField();
        y = new JTextField();
        button1 = new JButton();
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();

        //======== this ========
        setResizable(false);
        setMinimumSize(new Dimension(300, 200));
        var contentPane = getContentPane();

        //---- button1 ----
        button1.setText("Supprimer");
        button1.setBackground(UIManager.getColor("CodeWithMe.AccessDisabled.accessDot"));
        button1.setForeground(Color.red);
        button1.addActionListener(e -> supprimer(e));

        //---- label1 ----
        label1.setText("x");

        //---- label2 ----
        label2.setText("y");

        //---- label3 ----
        label3.setText("Veuillez entrer les coordonn\u00e9s de l'objet souhait\u00e9");
        label3.setFont(new Font("Segoe UI", Font.BOLD, 14));

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(14, 14, 14)
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(label3, GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(x, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(label2)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(y, GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                            .addGap(27, 27, 27)
                            .addComponent(button1, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label3)
                    .addGap(33, 33, 33)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label2)
                        .addComponent(label1)
                        .addComponent(x, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(y, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(button1))
                    .addContainerGap(109, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - zahar jihane
    private JTextField x;
    private JTextField y;
    private JButton button1;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
