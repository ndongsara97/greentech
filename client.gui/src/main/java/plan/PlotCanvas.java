package plan;

import entity.Anemometer;
import entity.RainSensor;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class PlotCanvas extends JPanel {
    private Anemometer[] anemometers;
    private RainSensor[] rain_sensors;

    public PlotCanvas(Anemometer[] anemometers, RainSensor[] rain_sensors) {
        super();
        this.anemometers = anemometers;
        this.rain_sensors = rain_sensors;
    }
    @Override
    public void paint(Graphics g) {
        Graphics2D graphic2d = (Graphics2D) g;
        graphic2d.setColor(Color.GREEN);
        graphic2d.fillRect(0, 0, 550, 250);
        File pathToPluie = new File(System.getenv("greenwatch")+"/etc/icons8-iot-64.png");
        File pathToAnemometre = new File(System.getenv("greenwatch")+"/etc/icons8-wind-64.png");
        File background = new File(System.getenv("greenwatch")+"/etc/parcelle.png");
        Image imagePluie = null;
        Image imageAnemometre = null;
        Image backgroundImage = null;
        try {
            imagePluie = ImageIO.read(pathToPluie);
            imageAnemometre = ImageIO.read(pathToAnemometre);
            backgroundImage = ImageIO.read(background);
        } catch (IOException e) {
            e.printStackTrace();
        }

        graphic2d.drawImage(backgroundImage, 0, 0, null);

        for (Anemometer a:anemometers) {
            graphic2d.drawImage(imageAnemometre,a.getX(),a.getY(),23,23,null);
        }for (RainSensor a:rain_sensors) {
            graphic2d.drawImage(imagePluie,a.getX(),a.getY(),23,23,null);
        }
//        for (Panel a:panels) {
//            graphic2d.drawImage(imagePluie,a.getX(),a.getY(),23,23,null);
//        }
    }

    public static void main(String[] args) {
//        JFrame frame = new JFrame("Demo");
//        frame.add(new ParcelleCanvas(null));
//        frame.setSize(550, 250);
//        frame.setVisible(true);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
