package plan;

import entity.Anemometer;
import entity.RainSensor;
import service.AnemometerService;
import service.SensorService;
import service.PlotService;
import javax.swing.*;
import java.awt.event.ActionEvent;


public class ObjectConnectedForm extends JFrame {

    private Anemometer[] anemometers;
    private RainSensor[] rain_sensors;
    private int plot_id ;
    private final AnemometerService anemometerService;
    private final SensorService sensorService;
    public ObjectConnectedForm(Anemometer[] anemometers, RainSensor[] rain_sensors, int plot_id) {
       anemometerService = new AnemometerService();
       sensorService = new SensorService();
        this.anemometers = anemometers;
        this.rain_sensors = rain_sensors;
        this.plot_id = plot_id;
        this.initComponents();
        this.initCombobox();
    }

    private boolean validateForm(String x, String y, String taille){
        try {
           float taillef = Float.parseFloat(taille);
            int xi =Integer.parseInt(x);
            int yi= Integer.parseInt(y);
            if(xi<0 || xi >530){
                JOptionPane.showMessageDialog(this,
                        "x entre 0 et 530",
                        "Format Erreur",
                        JOptionPane.ERROR_MESSAGE);
                return false;
            }
            if(yi<0 || yi >230){
                JOptionPane.showMessageDialog(this,
                        "y entre 0 et 230",
                        "Format Erreur",
                        JOptionPane.ERROR_MESSAGE);
                return false;
            }
            return true;
        }catch(NumberFormatException exception){
            JOptionPane.showMessageDialog(this,
                    "erreur dans le format de données, précision : taille => Réel \n,x => Entier,\n y => Entier",
                    "Format Erreur",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    private void insert(ActionEvent e) {
        if(!validateForm(x.getText(),y.getText(),taille.getText())){
            return;
        }
        if((PlotMenuGui.rain_sensors.length + PlotMenuGui.anemometers.length) >= PlotService.MAXCONNECTEDOBJECTS){
            JOptionPane.showMessageDialog(this,
                    "Parcelle pleine",
                    "Erreur de capacité : MAX = " + PlotService.MAXCONNECTEDOBJECTS,
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(PlanPanel.plotService.isObjectConnectedInXY(Integer.parseInt(x.getText()),Integer.parseInt(y.getText()),PlotMenuGui.anemometers,PlotMenuGui.rain_sensors)){
            JOptionPane.showMessageDialog(this,
                    "un objet connecté est déja dans cette position",
                    "Position Erreur",
                    JOptionPane.ERROR_MESSAGE);
        }else{
            if (objectConnected.getSelectedItem().toString().equals("Anémomètre")){
                anemometerService.AddAnemometer(new Anemometer(-1,plot_id,Integer.parseInt(x.getText()),Integer.parseInt(y.getText()),Float.parseFloat(taille.getText())));
                JOptionPane.showMessageDialog(this,
                        "Anémometre placé sur la parcelle avec succès ");
                PlotMenuGui.refreshData();
            }else{
                sensorService.AddSensor(new RainSensor(-1,plot_id,Float.parseFloat(taille.getText()),Integer.parseInt(x.getText()),Integer.parseInt(y.getText())));
                JOptionPane.showMessageDialog(this,
                        "Capteur de Pluie placé sur la parcelle avec succès ");
                PlotMenuGui.refreshData();
            }
        }
    }

    private void initCombobox(){
        objectConnected.addItem("Anémomètre");
        objectConnected.addItem("Capteur de pluie");
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - zahar jihane
        taille = new JTextField();
        x = new JTextField();
        y = new JTextField();
        label1 = new JLabel();
        objectConnected = new JComboBox();
        label2 = new JLabel();
        label3 = new JLabel();
        insert = new JButton();

        //======== this ========
        var contentPane = getContentPane();

        //---- label1 ----
        label1.setText("taille");

        //---- label2 ----
        label2.setText("x");

        //---- label3 ----
        label3.setText("y");

        //---- insert ----
        insert.setText("Inserer");
        insert.addActionListener(e -> insert(e));

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(label1)
                        .addComponent(label2)
                        .addComponent(label3))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addComponent(taille, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                        .addComponent(y, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                        .addComponent(x, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 90, Short.MAX_VALUE)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addComponent(objectConnected, GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                        .addComponent(insert, GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE))
                    .addGap(38, 38, 38))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(taille, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label1)
                        .addComponent(objectConnected, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label2)
                                .addComponent(x, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(30, 30, 30)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(y, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(label3))
                            .addContainerGap(86, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                            .addGap(18, 18, Short.MAX_VALUE)
                            .addComponent(insert)
                            .addGap(78, 78, 78))))
        );
        pack();
        setLocationRelativeTo(getOwner());
        this.setVisible(true);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - zahar jihane
    private JTextField taille;
    private JTextField x;
    private JTextField y;
    private JLabel label1;
    private JComboBox objectConnected;
    private JLabel label2;
    private JLabel label3;
    private JButton insert;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
