#!/bin/bash
scp ${repo_local}/scripts/vm_scripts/start-server.sh greentech@172.31.249.43:user/local/scripts
scp ${repo_local}/scripts/vm_scripts/kill-server.sh greentech@172.31.249.43:user/local/scripts
ssh greentech@172.31.249.43 chmod u+x user/local/scripts/start-server.sh user/local/scripts/kill-server.sh