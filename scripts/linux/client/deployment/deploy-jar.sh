#!/bin/bash
mvn package -DoutputDirectory=${greenwatch}/lib
cp ${repo_local}/client.cli/target/client.cli-1.0.0-SNAPSHOT-jar-with-dependencies.jar ${greenwatch}/lib
cp ${repo_local}/client.gui/target/client.gui-1.0.0-SNAPSHOT-jar-with-dependencies.jar ${greenwatch}/lib