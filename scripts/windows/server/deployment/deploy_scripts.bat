@ECHO OFF
CLS
for /f %%S in (..\..\..\..\etc\info-scripts.properties) do set %%S
scp %repo_local%\scripts\vm_scripts\start-server.sh %ip_server%:user/local/scripts
scp %repo_local%\scripts\vm_scripts\kill-server.sh %ip_server%:user/local/scripts
ssh %ip_server% chmod u+x user/local/scripts/start-server.sh user/local/scripts/kill-server.sh
cmd /k