package common;

import anemometer.MeasuresInsertCli;
import offers.OfferInsertCli;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import panel.PanelInsertCli;
import stats.StatsHistoryCli;
import stats.StatsInsertCli;

public class CliMain {
    public static void main(String[] args){
        Logger mainLog = LogManager.getLogger(CliMain.class);

        Options options = new Options();

        options.addRequiredOption("type", "type", true, "Starts selected cli");
        Option trace = new Option("t", "trace", false, "Set logger level to trace");
        options.addOption(trace);

        CommandLine cmd;
        @SuppressWarnings("deprecation")
        CommandLineParser parser = new BasicParser();
        HelpFormatter helper = new HelpFormatter();

        try {
            cmd = parser.parse(options, args);
            if(cmd.hasOption("t")){
                Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.TRACE);
                mainLog.trace("Trace mode activated");
            }
            Option cli = cmd.getOptions()[0];

            switch (cli.getValue()) {
                case "stats_insert" -> new StatsInsertCli();
                case "stats_history" -> new StatsHistoryCli();
                case "insert_panel" -> new PanelInsertCli();
                case "offer_insert" -> new OfferInsertCli();
                case "measures_insert" -> new MeasuresInsertCli();
                default -> mainLog.error("Invalid command");
            }
        } catch (ParseException e) {
            mainLog.error("Error when retrieving start arguments");
            helper.printHelp("Arguments needed :", options);
            System.exit(0);
        }
    }
}