package offers;

import connection.queries.QueryDecoder;
import connection.queries.QueryEncoder;
import connection.socket.SocketManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OfferInsertCli {
    public OfferInsertCli(){
        Logger offerLog = LogManager.getLogger(OfferInsertCli.class);
        offerLog.info("Start of OfferInsert");
        new SocketManager().startConnection("172.31.249.43", 6666);
        QueryEncoder encoder = new QueryEncoder();
        QueryDecoder decoder = new QueryDecoder();
try {
    encoder.insertOffre(
            "OffreTest",
            "5",
            "Avocat",
            "association",
            "2022-05-13",
            "2022-05-18"
    );

} catch (Exception e) {
    e.printStackTrace();
}
    }


    public static void main(String[] args) {
        new OfferInsertCli();
    }
}