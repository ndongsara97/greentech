package stats;

import connection.queries.QueryDecoder;
import connection.queries.QueryEncoder;
import connection.socket.SocketManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class StatsInsertCli {
    public StatsInsertCli(){
        Logger insertLog = LogManager.getLogger(StatsInsertCli.class);
        insertLog.info("Start of StatsInsertCli");
        new SocketManager().startConnection("172.31.249.43", 6666);
        QueryEncoder encoder = new QueryEncoder();
        QueryDecoder decoder = new QueryDecoder();

        //Reads stats_data.json file
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(System.getenv("greenwatch")+"/etc/stats_data.json")));
        } catch (FileNotFoundException e) {
            insertLog.error("Error trying to read json file", e);
        }

        //Converts file to String
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            insertLog.error("Error while reading json file");
        }
        String jsonQuery = sb.toString();
        insertLog.trace(jsonQuery);
        //Executes insert query using the data in the json file
        String[][] dataToInsert = decoder.convertDataToArray(jsonQuery,"data");
        for (int i = 0; i < dataToInsert.length; i++) {
            encoder.insertStats(dataToInsert[i][0], dataToInsert[i][1], dataToInsert[i][2]);

            SocketManager.receiveMessage();

            try {
                Thread.sleep(1); //Gives the server the time to process the previous queries
            } catch (InterruptedException e) {
                insertLog.error("Error while pausing Thread",e);
            }
        }
    }

    public static void main(String[] args) {
        new StatsInsertCli();
    }
}
