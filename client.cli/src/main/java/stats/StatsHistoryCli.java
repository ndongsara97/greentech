package stats;

import connection.queries.QueryDecoder;
import connection.queries.QueryEncoder;
import connection.socket.SocketManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StatsHistoryCli {
    public StatsHistoryCli(){
        Logger readLog = LogManager.getLogger(StatsHistoryCli.class);
        readLog.info("Client started");
        new SocketManager().startConnection("172.31.249.43",6666);

        QueryEncoder encoder = new QueryEncoder();
        QueryDecoder decoder = new QueryDecoder();

        encoder.selectStatsHistory("Nombre de capteurs","30");
        String response = SocketManager.receiveMessage();
        readLog.trace(response);
        readLog.info("Response received");

        String[][] data = decoder.convertDataToArray(response,"data");

        for (int i = 0; i < data.length; i++) {
            readLog.info(data[i][0]+" | "+data[i][1]);
        }
    }

    public static void main(String[] args) {
        new StatsHistoryCli();
    }
}
