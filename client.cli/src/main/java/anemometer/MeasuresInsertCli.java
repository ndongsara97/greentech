package anemometer;

import connection.queries.QueryEncoder;
import connection.socket.SocketManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MeasuresInsertCli {
    public MeasuresInsertCli() {
        Logger anemometerLog = LogManager.getLogger(MeasuresInsertCli.class);
        anemometerLog.info("Start of MeasuresInsertCli");
        new SocketManager().startConnection("172.31.249.43", 6666);
        QueryEncoder encoder = new QueryEncoder();
        try {
            anemometerLog.info("insertion d'une mesure avec une direction errone");
            encoder.insertDataMeasures(10.00, "Dieu", 9.8, 7);
            anemometerLog.trace("""
                    Windspeed = 10.00
                    WindDirection = Dieu
                    temperature = 9.8
                    """);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new MeasuresInsertCli();
    }
}