package connection.queries;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QueryDecoderTest {
    QueryDecoder decoder = new QueryDecoder();

    @Test
    public void testConvertDataToArray3Columns(){
        String entry = "{\"data\":\n" +
                "    [\n" +
                "        [\"Ronan\",\"Mac pour les nuls\",\"2022\"],\n" +
                "        [\"Thib\",\"Reussir son semestre en 1 soiree\", \"1\"],\n" +
                "    ]\n" +
                "}";
        String[][] expected = {
                {"Ronan","Mac pour les nuls","2022"},
                {"Thib","Reussir son semestre en 1 soiree","1"}
        };
        Assertions.assertArrayEquals(expected,decoder.convertDataToArray(entry,"data"));
    }

    @Test
    public void testConvertDataToArrayWith1Column(){
        String entry = "{\"data\":\n" +
                "    [\n" +
                "        [\"Ronan\"],\n" +
                "        [\"Thib\"],\n" +
                "        [\"Enzo\"],\n" +
                "        [\"Jihane\"],\n" +
                "        [\"Sara\"],\n" +
                "    ]\n" +
                "}";
        String[][] expected = {
                {"Ronan"},
                {"Thib"},
                {"Enzo"},
                {"Jihane"},
                {"Sara"}
        };
        Assertions.assertArrayEquals(expected,decoder.convertDataToArray(entry,"data"));
    }

    @Test
    public void testGetModifiedLines(){
        String entry = "{\"modified_lines\" : \"5\"}";
        Assertions.assertEquals(5,decoder.getModifiedLines(entry));
    }

    @Test
    public void testGetOperationUpdate(){
        String entry =
                "{\"data\":\n" +
                "    [\n" +
                "        [\"Ronan\"],\n" +
                "        [\"Thib\"],\n" +
                "        [\"Enzo\"],\n" +
                "        [\"Jihane\"],\n" +
                "        [\"Sara\"]\n" +
                "    ],\n" +
                "\"operation\":\"update\""+
                "}";
        Assertions.assertEquals("update",decoder.getOperation(entry));
    }
}
