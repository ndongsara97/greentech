package util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CheckFormatTest {

    @Test
    public void testIsIntWithText(){
        String entry = "sample text";
        boolean expected = false;
        new CheckFormat();
        Assertions.assertEquals(expected,CheckFormat.isInt(entry));
    }

    @Test
    public void testIsIntWithInt(){
        String entry = "324";
        boolean expected = true;
        new CheckFormat();
        Assertions.assertEquals(expected,CheckFormat.isInt(entry));
    }

    @Test
    public void testIsIntWithNull(){
        String entry = null;
        boolean expected = false;
        new CheckFormat();
        Assertions.assertEquals(expected,CheckFormat.isInt(entry));
    }

    @Test
    public void testIsIntWithFloat(){
        String entry = "3.56";
        boolean expected = false;
        new CheckFormat();
        Assertions.assertEquals(expected,CheckFormat.isInt(entry));
    }
}
