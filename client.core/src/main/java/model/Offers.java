package model;

import java.util.Date;

public class Offers {

    private String reference;
    private int remise;
    private Date date_debut;
    private Date date_fin;



    public Offers(String reference,int remise,Date date_debut,Date date_fin){
        this.reference = reference;
        this.remise=remise;
        this.date_debut=date_debut;
        this.date_fin=date_fin;
    }

}
