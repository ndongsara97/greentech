package model;

public class Product {
    private String nom_produit;
    private double prix_init;

    public Product(String name) {

        setNomProduit(nom_produit);
        setPrixInit(prix_init);
    }

    public String getNomProduit(){
        return this.nom_produit;
    }
    public void setNomProduit(String nom_produit) {
        this.nom_produit = nom_produit;
    }
    public double getNPrixInit(){
        return this.prix_init;
    }
    public void setPrixInit(double prix_init) {
        this.prix_init = prix_init;
    }


}
