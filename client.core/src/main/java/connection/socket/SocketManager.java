package connection.socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.net.Socket;

public class SocketManager {
    private static Socket clientSocket;
    private static PrintWriter output;
    private static BufferedReader input;
    private static final Logger socketLog = LogManager.getLogger(SocketManager.class);
    private static boolean connectingIssue = true;

    public static boolean startConnection(String ip, int port){
        try {
            clientSocket = new Socket(ip, port);
            output = new PrintWriter(clientSocket.getOutputStream(),true);
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            socketLog.info("Connected to server on "+ip+":"+port);
            return true;
        } catch (IOException ioe){
            socketLog.error("Unable to connect to server, check if the server is online");
            return false;
        }
    }

    public static void sendMessage(String jsonRequest){
        output.println(jsonRequest);
    }

    public static String receiveMessage(){
        try {
            return input.readLine();
        } catch (Exception e){
            if(connectingIssue) {
                //Prints error only once
                socketLog.error("Error when trying to receive data from inputstream", e);
                connectingIssue = false;
            }
            return null;
        }
    }

    public static boolean isReady(){
        try {
            return input.ready();
        } catch (IOException ioe){
            socketLog.error("Error when trying to get inputstream status",ioe);
        }
        return false;
    }

    public static void disconnect(){
        try {
            input.close();
            output.close();
            clientSocket.close();
            socketLog.info("Disconnection successful");
        } catch (IOException ioe){
            socketLog.error("Error when trying to close socket connection",ioe);
        }
        System.exit(0);
    }
}