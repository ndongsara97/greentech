package connection.queries;

import org.json.JSONArray;
import org.json.JSONObject;

public class QueryDecoder {

    public String getOperation(String response){
        JSONObject json = new JSONObject(response);
        return json.getString("operation");
    }

    public String[][] convertDataToArrayHandleEmptyJson(String jsonStr, String key){
        if(jsonStr==null){
            return new String[0][0];
        }
        JSONObject jsonObj = new JSONObject(jsonStr);
        JSONArray lines = jsonObj.getJSONArray(key);

        int rowNb = lines.length();
        if(rowNb==0){
            return new String[0][0];
        }
        int columnNb = lines.getJSONArray(0).length();
        String[][] data = new String[rowNb][columnNb];

        JSONArray singleLine;
        for (int i = 0; i < rowNb; i++) {
            singleLine = lines.getJSONArray(i);
            for (int j = 0; j < columnNb; j++) {
                data[i][j] = singleLine.getString(j);
            }
        }
        return data;
    }
    //Convert string in json format to multi-dimensional array
    public String[][] convertDataToArray(String jsonStr, String key){
        JSONObject jsonObj = new JSONObject(jsonStr);
        JSONArray lines = jsonObj.getJSONArray(key);

        int rowNb = lines.length();
        int columnNb = lines.getJSONArray(0).length();
        String[][] data = new String[rowNb][columnNb];

        JSONArray singleLine;
        for (int i = 0; i < rowNb; i++) {
            singleLine = lines.getJSONArray(i);
            for (int j = 0; j < columnNb; j++) {
                data[i][j] = singleLine.getString(j);
            }
        }
        return data;
    }

    public int getModifiedLines(String jsonStr){
        JSONObject jsonObj = new JSONObject(jsonStr);
        return jsonObj.getInt("modified_lines");
    }
}