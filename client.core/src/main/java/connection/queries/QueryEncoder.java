package connection.queries;

import connection.socket.SocketManager;
import org.json.JSONObject;

public class QueryEncoder {

    public void selectOperation(String operation){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation",operation);
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void selectAnemometer(String operation, int id){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation",operation);
        jsonQuery.put("idAnemometer",id);
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void insertProduct(String product_name, String price){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "insert_product");
        jsonQuery.put("product_name", product_name);
        jsonQuery.put("price", price);
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void insertStats(String date, String measure, String value){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation","stats_insert");
        jsonObject.put("date",date);
        jsonObject.put("measure",measure);
        jsonObject.put("value",value);
        SocketManager.sendMessage(jsonObject.toString());
    }

    public void selectStatsHistory(String measure, String interval){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation","stats_history");
        jsonObject.put("measure",measure);
        jsonObject.put("interval",interval);
        SocketManager.sendMessage(jsonObject.toString());
    }

    public void selectStatsComparison(String measureX, String measureY) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation","stats_comparison");
        jsonObject.put("measure_x",measureX);
        jsonObject.put("measure_y",measureY);
        SocketManager.sendMessage(jsonObject.toString());
    }

    public void insertOffre(String name_offre, String remise, String product_name,String type_client,String date_debut,String date_end){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation","insert_offre");
        jsonObject.put("name_offre",name_offre);
        jsonObject.put("remise",remise);
        jsonObject.put("product_name",product_name);
        jsonObject.put("type_client",type_client);
        jsonObject.put("date_debut",date_debut);
        jsonObject.put("date_end",date_end);
        SocketManager.sendMessage(jsonObject.toString());
    }



    public void updateOffres(String id,String name_offre, String remise, String product_name,String type_client,String date_debut,String date_end){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation","update_offres");
        jsonObject.put("id",id);
        jsonObject.put("name_offre",name_offre);
        jsonObject.put("remise",remise);
        jsonObject.put("product_name",product_name);
        jsonObject.put("type_client",type_client);
        jsonObject.put("date_debut",date_debut);
        jsonObject.put("date_end",date_end);
        SocketManager.sendMessage(jsonObject.toString());
    }

    public void updateProduct(String id,String product_name, String price){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation","update_product");
        jsonObject.put("id",id);
        jsonObject.put("price",price);
        jsonObject.put("product_name",product_name);

        SocketManager.sendMessage(jsonObject.toString());
    }

    public void insertAnemometer(int idAnemometer,int idParcelle,double size,int x, int y, boolean sensorOn, int lastGust) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation", "insert_anemometer");
        jsonObject.put("idAnemometer", idAnemometer);
        jsonObject.put("idParcelle", idParcelle);
        jsonObject.put("size", size);
        jsonObject.put("x", x);
        jsonObject.put("y", y);
        jsonObject.put("sensorOn", sensorOn);
        jsonObject.put("lastGust", lastGust);
        SocketManager.sendMessage(jsonObject.toString());
    }

    public void insertAnemometerWithoutLastGust(int idAnemometer,int idParcelle,double size,int x, int y, boolean sensorOn) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation", "insert_anemometer");
        jsonObject.put("idAnemometer", idAnemometer);
        jsonObject.put("idParcelle", idParcelle);
        jsonObject.put("size", size);
        jsonObject.put("x", x);
        jsonObject.put("y", y);
        jsonObject.put("sensorOn", sensorOn);
        SocketManager.sendMessage(jsonObject.toString());
    }

    public void insertDataMeasures(double windSpeed, String windDirection, double temperature, int idAnemometer){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "insert_data_measure");
        jsonQuery.put("wind_speed", windSpeed);
        jsonQuery.put("wind_direction", windDirection);
        jsonQuery.put("temperature", temperature);
        jsonQuery.put("id_anemometer", idAnemometer);
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void selectPlots(int idParcels, String energy){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "select_parcels_by_id");
        jsonQuery.put("id_parcelle", idParcels);
        jsonQuery.put("energy", energy);
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void setFirstIdPanel(){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "setFirstIdPanel");
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void selectExitsOffres(String name_offre){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "select_offres_exists");
        jsonQuery.put("name_offre", name_offre);
        SocketManager.sendMessage(jsonQuery.toString());

    }

    public void deleteOffres(String id){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "delete_offres");
        jsonQuery.put("id", id);
        SocketManager.sendMessage(jsonQuery.toString());

    }
    public void deleteProduct(String id){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "delete_product");
        jsonQuery.put("id", id);
        SocketManager.sendMessage(jsonQuery.toString());

    }

    public void insertPanel(int idPanel, float x, float y, int idParcels){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation","insert_panel");
        jsonObject.put("id_panel",idPanel);
        jsonObject.put("x",x);
        jsonObject.put("y",y);
        jsonObject.put("id_parcels", idParcels);
        SocketManager.sendMessage(jsonObject.toString());
    }

    public void updateSensor(int id_anemometer, boolean sensorOn) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("operation","update_sensor");
        jsonObject.put("id_anemometer",id_anemometer);
        jsonObject.put("sensorOn",sensorOn);
        SocketManager.sendMessage(jsonObject.toString());
    }
}