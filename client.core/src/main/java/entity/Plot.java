package entity;

public class Plot {

    private int id_plot,lenght,width;
    private float latitude,longitude,area,humidity_level,sunshine_rate;

    public Plot(int id_plot, float latitude, float longitude, float sunshine_rate, float area, int lenght, int width, float humidity_level) {
        this.id_plot = id_plot;
        this.latitude = latitude;
        this.longitude = longitude;
        this.sunshine_rate = sunshine_rate;
        this.area = area;
        this.lenght = lenght;
        this.width = width;
        this.humidity_level=humidity_level;
    }

    @Override
    public String toString() {
        return "Parcelle{" +
                "parcelle=" + id_plot +
                ", latitude=" + latitude +
                ", superficie=" + area +
                ", longitude=" + longitude +
                ", taux d'ensoleillement=" + sunshine_rate +
                ", longueur=" + lenght +
                ", largeur=" + width +
                ", taux d'humidité=" + humidity_level +
                '}';
    }

    public String getBasicInfo(){
        return  "<html>" +
                " <strong>parcelle</strong> :" + id_plot +
                "<br> <strong>taux d'ensoleillement</strong> :" + sunshine_rate + "%"+
                " <br><strong>superficie</strong> :" + area + " mètre carré"+
                "<br> <strong>taux d'humidité</strong> :" + humidity_level +"%"+
                "</html>";
    }

    public int getId_plot() {
        return id_plot;
    }
    public void setId_plot(int id_plot) {
        this.id_plot = id_plot;
    }
    public float getLatitude() {
        return latitude;
    }
    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
    public float getLongitude() {
        return longitude;
    }
    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
    public float getSunshine_rate() {
        return sunshine_rate;
    }
    public void setSunshine_rate(float sunshine_rate) {
        this.sunshine_rate = sunshine_rate;
    }
    public float getArea() {return area;}
    public float getHumidity_level() {return humidity_level;}
    public int getLenght() {return lenght;}
    public int getWidth() {return width;}
    public void setArea(float area) {this.area = area;}
    public void setHumidity_level(float humidity_level) {this.humidity_level = humidity_level;}
    public void setLenght(int lenght) {this.lenght = lenght;}
    public void setWidth(int width) {this.width = width;}
}
