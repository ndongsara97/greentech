package entity;

public class Anemometer {

    private int id_anemometer,id_plot,x,y;
    private float capacity;

    public Anemometer() {}

    public Anemometer(int id_anemometer, int id_plot, int x, int y, float capacity) {
        this.id_anemometer = id_anemometer;
        this.id_plot = id_plot;
        this.x = x;
        this.y = y;
        this.capacity = capacity;
    }

    public Anemometer(int id_anemometer, int id_plot, float capacity) {
        this.id_anemometer = id_anemometer;
        this.id_plot = id_plot;
        this.capacity = capacity;
        this.x = 0;
        this.y = 0;
    }

    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getId_anemometer() {
        return id_anemometer;
    }
    public void setId_anemometer(int id_anemometer) {
        this.id_anemometer = id_anemometer;
    }
    public int getId_plot() {
        return id_plot;
    }
    public void setId_plot(int id_plot) {this.id_plot = id_plot;}
    public float getCapacity() {
        return capacity;
    }
    public void setCapacity(float capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Anemometre{" +
                "id_anemometre=" + id_anemometer +
                ", id_parcelle=" + id_plot +
                ", x=" + x +
                ", y=" + y +
                ", capacité=" + capacity +
                '}';
    }
}

