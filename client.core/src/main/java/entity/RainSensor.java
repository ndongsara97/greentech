package entity;

public class RainSensor {

    private int id_sensor,id_plot,x,y;
    private float capacity;

    public RainSensor() {}

    public RainSensor(int id_sensor, int id_plot, float capacity, int x, int y) {
        this.id_sensor = id_sensor;
        this.id_plot = id_plot;
        this.capacity = capacity;
        this.x = x;
        this.y = y;
    }

    public int getId_sensor() {
        return id_sensor;
    }
    public void setId_sensor(int id_sensor) {
        this.id_sensor = id_sensor;
    }
    public int getId_plot() {
        return id_plot;
    }
    public void setId_plot(int id_plot) {
        this.id_plot = id_plot;
    }
    public float getCapacity() {
        return capacity;
    }
    public void setCapacity(float size) {
        this.capacity = capacity;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "CapteurPluie{" +
                "id_capteur=" + id_sensor +
                ", id_parcelle=" + id_plot +
                ", capacité=" + capacity +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}

