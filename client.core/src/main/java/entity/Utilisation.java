package entity;

public class Utilisation {

    private int id_utilisation,id_plot;
    private String name,type_utilisation;

    public Utilisation(int id_utilisation, int id_plot, String name, String type_utilisation) {
        this.id_utilisation = id_utilisation;
        this.id_plot = id_plot;
        this.name = name;
        this.type_utilisation = type_utilisation;
    }

    public Utilisation() {
        this.id_utilisation = -1;
        this.type_utilisation = "null";
        this.id_plot = -1;
        this.name = "null";
    }

    public int getId_utilisation() {
        return id_utilisation;
    }
    public void setId_utilisation(int id_utilisation) {
        this.id_utilisation = id_utilisation;
    }
    public int getId_plot() {
        return id_plot;
    }
    public void setId_plot(int id_plot) {
        this.id_plot = id_plot;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getType_utilisation() {
        return type_utilisation;
    }
    public void setType_utilisation(String type_utilisation) {
        this.type_utilisation = type_utilisation;
    }

    @Override
    public String toString() {
        return "Utilisation{" +
                "idUtilisation=" + id_utilisation +
                ", idParcelle=" + id_plot +
                ", nom='" + name + '\'' +
                ", typeUtilisation='" + type_utilisation + '\'' +
                '}';
    }
}