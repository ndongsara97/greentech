package service;

import connection.socket.SocketManager;
import entity.Utilisation;
import org.json.JSONObject;

public class UtilisationService {

    public UtilisationService(){}

    public void sendSelectUtilisationByPlotId(int plotId){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "select_utilisation_plot");
        jsonQuery.put("id_parcelle", String.valueOf(plotId));
       SocketManager.sendMessage(jsonQuery.toString());
    }

    public Utilisation getUtilisation( String[][] list_utilisation){
        Utilisation utilisation = new Utilisation() ;
        for (String [] utilisation_i:list_utilisation) {
            utilisation = new Utilisation(Integer.parseInt(utilisation_i[0]),Integer.parseInt(utilisation_i[1]),utilisation_i[2],utilisation_i[3]);

        }
        return utilisation;
    }

}

