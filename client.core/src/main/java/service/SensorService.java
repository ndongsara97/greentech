package service;

import connection.socket.SocketManager;
import entity.RainSensor;
import org.json.JSONObject;
import java.util.ArrayList;

public class SensorService {

    public SensorService(){}

    public void AddSensor(RainSensor sensor){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "insert_sensor");
        jsonQuery.put("id_parcelle", String.valueOf(sensor.getId_plot()));
        jsonQuery.put("taille", String.valueOf(sensor.getCapacity()));
        jsonQuery.put("x", String.valueOf(sensor.getX()));
        jsonQuery.put("y", String.valueOf(sensor.getY()));
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void sendSelectSensors(int plot_id){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "select_sensor_plot");
        jsonQuery.put("id_parcelle", String.valueOf(plot_id));
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public RainSensor[] getSensors(String[][] rain_sensors) {
        ArrayList<RainSensor> sensors = new ArrayList<>() ;
        for (String [] sensor:rain_sensors) {
            sensors.add(new RainSensor(Integer.parseInt(sensor[0]),Integer.parseInt(sensor[1]),Float.parseFloat(sensor[2]),Integer.parseInt(sensor[3]),Integer.parseInt(sensor[4])));
        }
        return sensors.toArray(new RainSensor[0]);
    }

    public void sendSelectSensorsXY(int plot_id, int x, int y){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "select_sensor_plot_x_y");
        jsonQuery.put("id_parcelle", String.valueOf(plot_id));
        jsonQuery.put("x", String.valueOf(x));
        jsonQuery.put("y", String.valueOf(y));
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void sendDeleteSensors(RainSensor sensor){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "delete_sensor");
        jsonQuery.put("id_capteur", String.valueOf(sensor.getId_sensor()));
        SocketManager.sendMessage(jsonQuery.toString());
    }
}


