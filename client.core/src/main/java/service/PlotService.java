package service;

import connection.socket.SocketManager;
import entity.Anemometer;
import entity.RainSensor;
import entity.Plot;
import org.json.JSONObject;
import java.util.ArrayList;

public class PlotService {

    public static final int MAXCONNECTEDOBJECTS = 30;

    public PlotService(){}

    public void sendSelect(){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "select_plot");
       SocketManager.sendMessage(jsonQuery.toString());
    }

    public Plot[] getAllPlot(String[][] list_plot){
        ArrayList<Plot> plots = new ArrayList<>();
        for (String [] plots_i:list_plot) {
            plots.add(new Plot(Integer.parseInt(plots_i[0]),Float.parseFloat(plots_i[1]),Float.parseFloat(plots_i[2]),Float.parseFloat(plots_i[3]),Float.parseFloat(plots_i[4]),Integer.parseInt(plots_i[5]),Integer.parseInt(plots_i[6]),Float.parseFloat(plots_i[7])));
        }
        return plots.toArray(new Plot[0]);
    }

    public boolean isObjectConnectedInXY(int x, int y, Anemometer[] anemometers, RainSensor[] rain_sensors){
        boolean found = false;
        for (Anemometer anemometer: anemometers) {
            if(anemometer.getX()==x && anemometer.getY()==y){
                found=true;
                break;
            }
        }
        for (RainSensor rain_sensor: rain_sensors) {
            if(rain_sensor.getX()==x && rain_sensor.getY()==y){
                found=true;
                break;
            }
        }
        return found;
    }
}