package service;

import connection.socket.SocketManager;
import entity.Anemometer;
import org.json.JSONObject;
import java.util.ArrayList;

public class AnemometerService {

    public AnemometerService(){}

    public void  AddAnemometer(Anemometer anemometer){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "insert_anemometer1");
        jsonQuery.put("id_parcelle", String.valueOf(anemometer.getId_plot()));
        jsonQuery.put("taille", String.valueOf(anemometer.getCapacity()));
        jsonQuery.put("x", String.valueOf(anemometer.getX()));
        jsonQuery.put("y", String.valueOf(anemometer.getY()));
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void sendSelectAnemometers(int plot_id){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "select_anemometer_plot");
        jsonQuery.put("id_parcelle", String.valueOf(plot_id));
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void sendDeleteAnemometers(Anemometer anemometer){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "delete_anemometer");
        jsonQuery.put("id_anemometre", String.valueOf(anemometer.getId_anemometer()));
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public void sendSelectAnemometersXY(int plot_id, int x, int y){
        JSONObject jsonQuery = new JSONObject();
        jsonQuery.put("operation", "select_anemometer_plot_x_y");
        jsonQuery.put("id_parcelle", String.valueOf(plot_id));
        jsonQuery.put("x", String.valueOf(x));
        jsonQuery.put("y", String.valueOf(y));
        SocketManager.sendMessage(jsonQuery.toString());
    }

    public Anemometer[] getAnemometers(String[][] anemometers_s) {
        ArrayList<Anemometer> anemometers = new ArrayList<>() ;
        for (String [] anemometer:anemometers_s) {
            anemometers.add(new Anemometer(Integer.parseInt(anemometer[0]),Integer.parseInt(anemometer[1]),Integer.parseInt(anemometer[3]),Integer.parseInt(anemometer[4]),Float.parseFloat(anemometer[2])));
        }
        return anemometers.toArray(new Anemometer[0]);
    }

}